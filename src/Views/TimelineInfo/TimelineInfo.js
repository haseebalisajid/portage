import Timeline from "react-calendar-timeline";
import randomColor from "randomcolor";
import CustomTabs from "../CustomTabs/CustomTabs";
import Box from "@mui/material/Box";

import Tooltip from "@mui/material/Tooltip";
import "react-calendar-timeline/lib/Timeline.css";
import "./TimelineInfo.css";
import moment from "moment";

const input = [
  {
    category: "diagnoses",
    title: "Mi",
  },
  {
    category: "tests",
    title: "IMAGING",
  },
  {
    category: "tests",
    title: "MRI",
  },
  {
    category: "diagnoses",
    title: "Mi",
  },
  {
    category: "body part",
    title: "BRAIN",
  },
  {
    category: "tests",
    title: "study",
  },
  {
    category: "tests",
    title: "AIRIS II magnetic resonance imaging",
  },
  {
    category: "body part",
    title: "T1",
  },
  {
    category: "body part",
    title: "Cortical sulci",
  },
  {
    category: "body part",
    title: "gyri",
  },
  {
    category: "body part",
    title: "cerebral",
  },
  {
    category: "body part",
    title: "cerebellar hemispheres",
  },
  {
    category: "diagnoses",
    title: "cerebellar hemispheres",
  },
  {
    category: "body part",
    title: "cerebral",
  },
  {
    category: "body part",
    title: "cerebral",
  },
  {
    category: "body part",
    title: "cervicocranial junction",
  },
  {
    category: "body part",
    title: "suprasellar cistern",
  },
  {
    category: "body part",
    title: "quadrigeminal plate cistern",
  },
  {
    category: "diagnoses",
    title: "polyp",
  },
  {
    category: "diagnoses",
    title: "retention cyst",
  },
  {
    category: "body part",
    title: "maxillary antrum",
  },
  {
    category: "body part",
    title: "mucous membrane",
  },
  {
    category: "body part",
    title: "frontal sinuses",
  },
  {
    category: "body part",
    title: "maxillary",
  },
  {
    category: "diagnoses",
    title: "mucous membrane reaction",
  },
  {
    category: "body part",
    title: "maxillary antrum",
  },
  {
    category: "body part",
    title: "frontal sinuses",
  },
  {
    category: "body part",
    title: "maxillary",
  },
  {
    category: "body part",
    title: "antrum",
  },
  {
    category: "diagnoses",
    title: "inflammatory debris",
  },
  {
    category: "body part",
    title: "sides",
  },
  {
    category: "tests",
    title: "Magnetic Resonance Imaging",
  },
  {
    category: "tests",
    title: "Unremarkable",
  },
  {
    category: "tests",
    title: "Sinus disease",
  },
  {
    category: "body part",
    title: "brain",
  },
  {
    category: "body part",
    title: "Sinus",
  },
  {
    category: "diagnoses",
    title: "Sinus disease",
  },
  {
    category: "body part",
    title: "brain",
  },
  {
    category: "body part",
    title: "brain",
  },
  {
    category: "medication",
  },
  {
    category: "procedure",
  },
];

export default function TimelineInfo() {
  let categories = input.map((item) => item.category);
  categories = [...new Set(categories)];
  const groupColorsObj = {};
  categories.forEach((category) => {
    groupColorsObj[category] = randomColor({ luminosity: "dark" });
  });

  const groups = categories.map((category) => {
    let obj = {};
    obj.id = category;
    obj.title = category;
    obj.stackItems = true;
    obj.color = groupColorsObj[category];
    return obj;
  });

  let count = 0;
  const items = input.map((item) => {
    count++;
    let obj = {};
    obj.id = count;
    obj.group = item.category;
    obj.title = item.title;
    obj.professional = "Amy Tucker";
    obj.itemProps = {
      style: { backgroundColor: groupColorsObj[item.category] },
    };
    obj.start_time = moment().add(count, "hour");
    obj.end_time = moment().add(count + 3, "hour");
    return obj;
  });

  const groupRenderer = ({ group }) => {
    return (
      <div style={{ color: group.color, fontWeight: "bold" }}>
        {group.title}
      </div>
    );
  };

  const itemRenderer = ({
    item,
    timelineContext,
    itemContext,
    getItemProps,
    group,
    getResizeProps,
  }) => {
    const { left: leftResizeProps, right: rightResizeProps } = getResizeProps();
    const backgroundColor = item.itemProps.style.backgroundColor || "red";
    const borderColor = item.itemProps.style.backgroundColor
      ? item.itemProps.style.backgroundColor
      : "red";
    return (
      <div
        {...getItemProps({
          style: {
            backgroundColor: backgroundColor,
            color: "white",
            borderColor,
            borderStyle: "solid",
            borderWidth: 1,
            borderRadius: 4,
            borderLeftWidth: itemContext.selected ? 3 : 1,
            borderRightWidth: itemContext.selected ? 3 : 1,
          },
        })}
        title={null}
      >
        <div
          style={{
            height: itemContext.dimensions.height,
            overflow: "hidden",
            paddingLeft: 3,
            textOverflow: "ellipsis",
            whiteSpace: "nowrap",
          }}
        >
          <Tooltip
            title={
              <div>
                <p>
                  <b>Operative report </b>
                  <br />
                  Lake Forest <br /> Orthopedic <br /> Amy Tucker
                </p>
                <p>
                  <b>Scope repair</b>
                  <br />
                  Knee, left
                </p>
              </div>
            }
          >
            <Box>{itemContext.title}</Box>
          </Tooltip>
        </div>
      </div>
    );
  };

  return (
    <>
      <CustomTabs />

      <Timeline
        groups={groups}
        items={items}
        groupRenderer={groupRenderer}
        defaultTimeStart={moment()}
        defaultTimeEnd={moment().add(5, "day")}
        itemRenderer={itemRenderer}
      ></Timeline>
    </>
  );
}
