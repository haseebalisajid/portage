import CustomTabs from "../CustomTabs/CustomTabs";
import Grid from "@mui/material/Grid";
import List from "@mui/material/List";
import { makeStyles } from "@mui/styles";
import ListItem from "@mui/material/ListItem";
import Typography from "@mui/material/Typography";

const useStyle = makeStyles({
  typography: {
    textDecoration: "underline",
  },
  items: {
    color: "#808080",
    padding: "0px",
  },
});

export default function Listing() {
  const classes = useStyle();
  const facilities = [
      "Lake Forest",
      "Orthopedic",
      "Associates",
      "Tri cities MRI Center",
    ],
    professionals = ["Ed lee", "George Mackie", "Amy Tucker"],
    recordTypes = [
      "Physical exam note",
      "MRI imaging report",
      "surgical report",
    ],
    recordYears = ["2021"];
  const symptoms = ["Left knee pain", "Swelling", "Bruise"],
    diagnoses = ["Contusion-thigh", "knee sprain", "Torn meniscus"],
    tests = ["Physical exam", "MRI - Left knee"],
    medications = ["Ibuprofen 10 mg", "Valium 5 mg 2x"],
    procedures = ["Knee scope surgery"];

  return (
    <>
      {" "}
      <CustomTabs />
      <Grid container spacing={1} justifyContent='space-between'>
        <Grid item>
          <Typography
            sx={{ mb: 1 }}
            variant='p'
            className={classes.typography}
            component='div'
          >
            Facilities
          </Typography>
          <List disablePadding>
            {facilities.map((item, i) => (
              <ListItem className={classes.items} key={i}>
                {item}
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item>
          <Typography
            sx={{ mb: 1 }}
            className={classes.typography}
            variant='p'
            component='div'
          >
            Professionals
          </Typography>
          <List disablePadding>
            {professionals.map((item, i) => (
              <ListItem className={classes.items} key={i}>
                {item}
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item>
          <Typography
            sx={{ mb: 1 }}
            className={classes.typography}
            variant='p'
            component='div'
          >
            Record Types
          </Typography>
          <List disablePadding>
            {recordTypes.map((item, i) => (
              <ListItem className={classes.items} key={i}>
                {item}
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item>
          <Typography
            sx={{ mb: 1 }}
            className={classes.typography}
            variant='p'
            component='div'
          >
            Record Years
          </Typography>
          <List disablePadding>
            {recordYears.map((item, i) => (
              <ListItem className={classes.items} key={i}>
                {item}
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item>
          <Typography
            sx={{ mb: 1 }}
            className={classes.typography}
            variant='p'
            component='div'
          >
            Symptoms
          </Typography>
          <List disablePadding>
            {symptoms.map((item, i) => (
              <ListItem className={classes.items} key={i}>
                {item}
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item>
          <Typography
            sx={{ mb: 1 }}
            className={classes.typography}
            variant='p'
            component='div'
          >
            Diagnoses
          </Typography>
          <List disablePadding>
            {diagnoses.map((item, i) => (
              <ListItem className={classes.items} key={i}>
                {item}
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item>
          <Typography
            sx={{ mb: 1 }}
            className={classes.typography}
            variant='p'
            component='div'
          >
            Tests
          </Typography>
          <List disablePadding>
            {tests.map((item, i) => (
              <ListItem className={classes.items} key={i}>
                {item}
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item>
          <Typography
            sx={{ mb: 1 }}
            className={classes.typography}
            variant='p'
            component='div'
          >
            Medications
          </Typography>
          <List disablePadding>
            {medications.map((item, i) => (
              <ListItem className={classes.items} key={i}>
                {item}
              </ListItem>
            ))}
          </List>
        </Grid>
        <Grid item>
          <Typography
            sx={{ mb: 1 }}
            className={classes.typography}
            variant='p'
            component='div'
          >
            Procedures
          </Typography>
          <List disablePadding>
            {procedures.map((item, i) => (
              <ListItem className={classes.items} key={i}>
                {item}
              </ListItem>
            ))}
          </List>
        </Grid>
      </Grid>
    </>
  );
}
