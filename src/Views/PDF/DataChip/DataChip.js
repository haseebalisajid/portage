import React from 'react'
import Chip from "@mui/material/Chip";
import FilterAltIcon from "@mui/icons-material/FilterAlt";
import './DataChip.css';

const DataChip = ({chipName,chipSelectionData}) => {

      //DUMMY TYPES of filters for testing
  const types = ["Diagnosis", "Procedure", "Medication", "Body Part", "Test"];
  
  const callerFunction=(data)=>{
    chipSelectionData(data);
  }

  return (
    <div className="chipsMain">
        <div className="chips">
            {types.map((dat)=>(
            <Chip className={chipName.includes(dat)?"singleChip2":"singleChip"} label={dat}  onClick={()=>callerFunction(dat)} />
            ))}
        </div>
        <div className="filterIcon">
            <FilterAltIcon fontSize="medium" />
        </div>
  </div>
  )
}

export default DataChip