import React from 'react'
import './DataColumn.css';


const DataColumn = ({showText,index,val,setShowText,sidebarClick,scale,addScale,setAddScale,zoomTo,jumpToHighlightArea}) => {
    
    const showTextFunction=(value)=>{
        setShowText(value);
    }

    const clickFunction=(data)=>{
        sidebarClick(data)
    }

    const zoomFunction=(zoomValue)=>{
        zoomTo(zoomValue);
    }

    const setScaleFunction=(scaleValue)=>{
        setAddScale(scaleValue)
    }

    const jumpHighlightAreas=(RequiredLightedAreas)=>{
        jumpToHighlightArea(RequiredLightedAreas)
    }

    return (
        <div
            className='showData'
            style={{
            border:
                showText !== null && showText === index
                ? "1px solid green"
                : "1px solid lightgray",
            borderBottom:
                showText !== null && showText === index
                ? "0"
                : "1px solid lightgray",
            }}
            tabIndex={index + 1}
            key={val.page + index}
            onClick={() => {
            if (showText == index) {
                showTextFunction(null);
            } else {
                showTextFunction(index);
            }
            clickFunction(val);
            if (addScale) {
                const changeScale = scale + 0.0001;
                zoomFunction(changeScale);
                setScaleFunction(false);
            } else {
                const changeScale = scale - 0.0001;
                zoomFunction(changeScale);
                setScaleFunction(true);
            }
            jumpHighlightAreas(lightedAreas);
            }}
        >
            <p className="textWrap"> 
            {val.text}
            </p>
        </div>
  )
}

export default DataColumn