//imports

import React, { useState, useEffect, useRef } from "react";
import CustomTabs from "../CustomTabs/CustomTabs";
import JumptoPage from "../../Components/JumpToPage/JumptoPage";
import CustomZoomPlugin from "../../Components/ZoomPlugin/CustomZoomPlugin";
import { useLocation } from "react-router-dom";
import { defaultLayoutPlugin } from "@react-pdf-viewer/default-layout";
import { pageNavigationPlugin } from "@react-pdf-viewer/page-navigation";
import { highlightPlugin, MessageIcon } from "@react-pdf-viewer/highlight";
import { highLightData, sampleData } from "../../utils/MockData";
import AWS from "aws-sdk";
import DataChip from "./DataChip/DataChip";
import SearchFiled from "../../Components/SearchFIeld/SearchFiled";
import DataColumn from "./DataColumn/DataColumn";
import TextAreaBox from "./TextAreaBox/TextAreaBox";
import PerPageNote from "./PerPageNote/PerPageNote";
import CircularProgress from "@mui/material/CircularProgress";
import Box from "@mui/material/Box";
import DrawCanvas from "../../Components/Canvas/DrawCanvasTest";
import useResizeObserver from "resize-observer-hook";
import autosize from "autosize";

import "./Pdf.css";
import {
  Icon,
  Viewer,
  Worker,
  Button,
  Tooltip,
  Position,
  ProgressBar,
  PrimaryButton,
} from "@react-pdf-viewer/core";

import "@react-pdf-viewer/core/lib/styles/index.css";
import "@react-pdf-viewer/highlight/lib/styles/index.css";
import "@react-pdf-viewer/default-layout/lib/styles/index.css";
//end imports

//env keys
const ACCESS_KEY_ID = process.env.REACT_APP_ACCESS_KEY;
const SECRET_ACCESS_KEY = process.env.REACT_APP_SECRET_KEY;


//start of component
export default function Pdf() {
  const pageNavigationPluginInstance = pageNavigationPlugin();
  const location = useLocation();
  //variables for the resize width of a div
  const [ref, width, height] = useResizeObserver();

  //states
  const [buffer, setBuffer] = useState([]);
  const [notes, setNotes] = useState([]);
  const [boundaryBox, setBoundaryBox] = useState([]);
  const [message, setMessage] = useState("");
  const [boundary, setBoundary] = useState("");
  const [jumpToPageNo, setJumpToPageNo] = useState(0);
  const [currentDoc, setCurrentDoc] = useState(null);
  const [filteredData, setFilteredData] = useState([]);
  const [search, setSearch] = useState("");
  const [show, setShow] = useState(false);
  const [filterData, setFilterData] = useState(sampleData);
  const [scale, setScale] = useState(1.15);
  const [addScale, setAddScale] = useState(false);
  const [showText, setShowText] = useState(null);
  const [lightedAreas, setLightedAreas] = useState([]);
  const [prevData, setPrevData] = useState([]);
  const [chipName, setChipName] = useState([]);
  const [textAreaData, setTextAreaData] = useState("");
  const [prevSelection, setPrevSelection] = useState([]);
  const [noteActive, setNoteActive] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);

  //pulgins declarations
  let customCanvasPluginInstance = DrawCanvas(() => filteredData);
  //custom zoom plugin to handle zoom
  const customZoomPluginInstance = CustomZoomPlugin();
  const { zoomTo } = customZoomPluginInstance;

  const notesContainerRef = useRef(null);
  const boundaryBoxRef = useRef(null);
  const textareaRef = useRef(null);
  const textareaRef2 = useRef(null);

  let noteId = notes.length;
  let boundaryBoxId = boundaryBox.length;
  const noteEles = new Map();

  //DUMMY TYPES of filters for testing
  const types = ["Diagnosis", "Procedure", "Medication", "Body Part", "Test"];

  useEffect(() => {
    AWS.config.update({
      region: "us-west-2",
      accessKeyId: ACCESS_KEY_ID,
      secretAccessKey: SECRET_ACCESS_KEY,
    });
    const s3 = new AWS.S3({
      params: { Bucket: "cedant-claims-extract" },
    });
    s3.getObject({ Key: "test_pdf_folder/sample.pdf" }, (err, data) => {
      if (err) console.log(err);
      else {
        console.log("data fetched!!");
        setBuffer(data.Body.buffer);
        setShow(true);
      }
    });
    return () => {
      noteEles.clear();
    };
  }, []);

  useEffect(() => {
    autosize(textareaRef);
    autosize(textareaRef2);
  }, []);

  const handleDocumentLoad = (e) => {
    setCurrentDoc(jumpToPageNo);
    jumpToPage(jumpToPageNo);
    if (currentDoc && currentDoc !== e.doc) {
      // User opens new document
      setNotes([]);
      setBoundaryBox([]);
    }
  };

  //function when user pressed enter key
  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      handleSearch();
    }
  };

  //function when search box value changes
  const handleChange = (e) => {
    setSearch(e.target.value);
    if (e.target.value.length === 0) {
      setFilterData(prevData);
    }
  };

  //handle textarea
  const handleTextArea = (e) => {
    setTextAreaData(e.target.value);
  };
  //function for search
  const handleSearch = () => {
    let data = filterData.filter((val) => {
      return val.text.toLowerCase().includes(search.toLowerCase());
    });
    setPrevData(filterData);
    setFilterData(data);
  };

  //this function draw canvas on the basis of the click from the rightside of the pdf

  const sidebarClick = (data) => {
    setPrevSelection([data]);
    if (prevSelection.length > 0) {
      if (textAreaData !== "") {
        let newArray = [...filterData];
        let objIndex = newArray.findIndex((obj) => obj === prevSelection[0]);
        newArray[objIndex].note = textAreaData;
        setFilterData(newArray);
      }
    }
    if (data.note) {
      setTextAreaData(data.note);
    } else {
      setTextAreaData("");
    }
    const found = filteredData.some(
      (val) => val.text === data.text && val.pageIndex === data.pageIndex
    );
    if (!found) {
      const highlightAreaData = highLightData.filter(
        (val) =>
          val.text === data.text &&
          val.highlightAreas.pageIndex === data.pageIndex
      );
      let matchingData = sampleData.filter(
        (val) => val.text === data.text && val.pageIndex === data.pageIndex
      );
      setLightedAreas(highlightAreaData);
      setFilteredData(matchingData);
    } else {
      setFilteredData([]);
      setLightedAreas([]);
    }
  };

  //function to filterout the data
  const getChipsRelatedData = (arr) => {
    let result = [];
    arr.map((dat) => {
      let output = sampleData.filter(
        (val) => val.type.toLowerCase() === dat.toLowerCase()
      );
      result.push(...output);
    });
    return result;
  };

  //getFilteredData on the basis of the chip selection
  const chipSelectionData = (name) => {
    if (chipName.includes(name)) {
      let exclude = chipName.filter((val) => val !== name);
      setChipName(exclude);
      let finalData = getChipsRelatedData(exclude);
      if (exclude.length == 0) {
        setFilterData(sampleData);
      } else {
        setPrevData(finalData);
        setFilterData(finalData);
      }
    } else {
      let chipArr = chipName;
      chipArr.push(name);
      setChipName(chipArr);
      let output = sampleData.filter(
        (val) => val.type.toLowerCase() === name.toLowerCase()
      );
      let finalData = getChipsRelatedData(chipArr);
      setPrevData(finalData);
      setFilterData(finalData);
    }
  };

  const onPageChange = (e) => {
    setCurrentPage(e.currentPage + 1);
    setNoteActive(true);
  };

  useEffect(() => {
    if (location && location?.state?.page) {
      setJumpToPageNo(location.state.page);
    }
  }, [location]);

  //functions for plugins
  const renderHighlightTarget = (props) => (
    <div
      style={{
        background: "#eee",
        display: "flex",
        position: "absolute",
        left: `${props.selectionRegion.left}%`,
        top: `${props.selectionRegion.top + props.selectionRegion.height}%`,
        transform: "translate(0, 8px)",
      }}
    >
      <Tooltip
        position={Position.TopCenter}
        target={
          <Button onClick={props.toggle}>
            <MessageIcon />
          </Button>
        }
        content={() => <div style={{ width: "100px" }}>Add a note</div>}
        offset={{ left: 0, top: -8 }}
      />
    </div>
  );

  const renderBoundaryBoxTarget = (props) => (
    <div
      style={{
        background: "#eee",
        display: "flex",
        position: "absolute",
        left: `${props.selectionRegion.left}%`,
        top: `${props.selectionRegion.top + props.selectionRegion.height}%`,
        transform: "translate(0, 8px)",
      }}
    >
      <Tooltip
        position={Position.TopCenter}
        target={
          <Button onClick={props.toggle}>
            <Icon size={16}>
              <path d="M0.500 7.498 L23.500 7.498 L23.500 16.498 L0.500 16.498 Z" />
              <path d="M3.5 9.498L3.5 14.498" />
            </Icon>
          </Button>
        }
        content={() => <div style={{ width: "100px" }}>Add Boundary Box</div>}
        offset={{ left: 0, top: -8 }}
      />
    </div>
  );

  const renderHighlightContent = (props) => {
    const addNote = () => {
      if (message !== "") {
        const note = {
          id: ++noteId,
          content: message,
          highlightAreas: props.highlightAreas,
          quote: props.selectedText,
        };
        setNotes(notes.concat([note]));
        props.cancel();
      }
    };

    return (
      <div
        style={{
          background: "#fff",
          border: "1px solid rgba(0, 0, 0, .3)",
          borderRadius: "2px",
          padding: "8px",
          position: "absolute",
          left: `${props.selectionRegion.left}%`,
          top: `${props.selectionRegion.top + props.selectionRegion.height}%`,
          zIndex: 1,
        }}
      >
        <div>
          <textarea
            rows={3}
            style={{
              border: "1px solid rgba(0, 0, 0, .3)",
            }}
            onChange={(e) => setMessage(e.target.value)}
          ></textarea>
        </div>
        <div
          style={{
            display: "flex",
            marginTop: "8px",
          }}
        >
          <div style={{ marginRight: "8px" }}>
            <PrimaryButton onClick={addNote}>Add</PrimaryButton>
          </div>
          <Button onClick={props.cancel}>Cancel</Button>
        </div>
      </div>
    );
  };
  const renderBoundaryBoxContent = (props) => {
    const addBoundarBox = () => {
      if (boundary !== "") {
        const note = {
          id: ++boundaryBoxId,
          content: boundary,
          highlightAreas: props.highlightAreas,
          quote: props.selectedText,
        };
        setBoundaryBox(boundaryBox.concat([note]));
        props.cancel();
      }
    };

    return (
      <div
        style={{
          background: "#fff",
          border: "1px solid rgba(0, 0, 0, .3)",
          borderRadius: "2px",
          padding: "8px",
          position: "absolute",
          left: `${props.selectionRegion.left}%`,
          top: `${props.selectionRegion.top + props.selectionRegion.height}%`,
          zIndex: 1,
        }}
      >
        <div>
          <textarea
            rows={3}
            style={{
              border: "1px solid rgba(0, 0, 0, .3)",
            }}
            onChange={(e) => setBoundary(e.target.value)}
          ></textarea>
        </div>

        <div
          style={{
            display: "flex",
            marginTop: "8px",
          }}
        >
          <div style={{ marginRight: "20px" }}>
            <PrimaryButton onClick={addBoundarBox}>Add</PrimaryButton>
          </div>
          <Button onClick={props.cancel}>Cancel</Button>
        </div>
      </div>
    );
  };

  const jumpToNote = (note) => {
    activateTab(3);
    const notesContainer = notesContainerRef.current;
    if (noteEles.has(note.id) && notesContainer) {
      notesContainer.scrollTop = noteEles
        .get(note.id)
        .getBoundingClientRect().top;
    }
  };
  const jumpToBoundaryBox = (note) => {
    console.log("hello");
    activateTab(4);
    const boundaryBoxContainer = boundaryBoxRef.current;
    if (noteEles.has(note.id) && boundaryBoxContainer) {
      boundaryBoxContainer.scrollTop = noteEles
        .get(note.id)
        .getBoundingClientRect().top;
    }
  };

  const renderHighlights = (props) => (
    <div>
      {notes.map((note) => (
        <React.Fragment key={note.id}>
          {note.highlightAreas
            .filter((area) => area.pageIndex === props.pageIndex)
            .map((area, idx) => (
              <div
                key={idx}
                style={Object.assign(
                  {},
                  {
                    background: "yellow",
                    opacity: 0.4,
                  },
                  props.getCssProperties(area, props.rotation)
                )}
                onClick={() => jumpToNote(note)}
              />
            ))}
        </React.Fragment>
      ))}
    </div>
  );
  const renderBoundaryBox = (props) => (
    <div>
      {boundaryBox.map((note) => (
        <React.Fragment key={note.id}>
          {note.highlightAreas
            .filter((area) => area.pageIndex === props.pageIndex)
            .map((area, idx) => (
              <div
                key={idx}
                style={Object.assign(
                  {},
                  {
                    background: "yellow",
                    opacity: 0.4,
                  },
                  props.getCssProperties(area, props.rotation)
                )}
                onClick={() => jumpToBoundaryBox(note)}
              />
            ))}
        </React.Fragment>
      ))}
    </div>
  );

  //end ---> functions for plugins

  // This function adds content for highlight in sidebar and on click functionality
  const sidebarNotes = (
    <div
      ref={notesContainerRef}
      style={{
        overflow: "auto",
        width: "100%",
      }}
    >
      {notes.length === 0 && (
        <div style={{ textAlign: "center" }}>There is no note</div>
      )}
      {notes.map((note) => {
        return (
          <div
            key={note.id}
            style={{
              borderBottom: "1px solid rgba(0, 0, 0, .3)",
              cursor: "pointer",
              padding: "8px",
            }}
            onClick={() => jumpToHighlightArea(note.highlightAreas[0])}
            ref={(ref) => {
              noteEles.set(note.id, ref);
            }}
          >
            {console.log(note.highlightAreas)}
            <blockquote
              style={{
                borderLeft: "2px solid rgba(0, 0, 0, 0.2)",
                fontSize: ".75rem",
                lineHeight: 1.5,
                margin: "0 0 8px 0",
                paddingLeft: "8px",
                textAlign: "justify",
              }}
            >
              {note.quote}
            </blockquote>
            {note.content}
          </div>
        );
      })}
    </div>
  );

  //HIGHLIGHTED SIDEBAR DATA
  // const boundaryBoxContent = (
  //   <div
  //     ref={boundaryBoxRef}
  //     style={{
  //       overflow: "auto",
  //       width: "100%",
  //     }}
  //   >
  //     {highLightData.map((note, index) => {
  //       return (
  //         <div
  //           key={note.text + index}
  //           style={{
  //             borderBottom: "1px solid rgba(0, 0, 0, .3)",
  //             cursor: "pointer",
  //             padding: "8px",
  //           }}
  //           onClick={() => {
  //             getSimilarData(note);
  //             if(addScale) {
  //               const changeScale=scale+0.0001
  //               zoomTo(changeScale)
  //               setAddScale(false);

  //             } else {

  //               const changeScale=scale-0.0001
  //               zoomTo(changeScale)
  //               setAddScale(true)
  //             }
  //             jumpToHighlightArea(note.highlightAreas);
  //           }}
  //           ref={(ref) => {
  //             noteEles.set(index, ref);
  //           }}
  //         >
  //           <blockquote
  //             style={{
  //               borderLeft: "2px solid rgba(0, 0, 0, 0.2)",
  //               fontSize: ".75rem",
  //               lineHeight: 1.5,
  //               margin: "0 0 8px 0",
  //               paddingLeft: "8px",
  //               textAlign: "justify",
  //             }}
  //           >
  //             {note.text}
  //           </blockquote>
  //           {note.text}
  //         </div>
  //       );
  //     })}
  //   </div>
  // );

  // this function adds water mark on the page

  const renderPage = (props) => (
    <>
      {props.canvasLayer.children}
      <div
        style={{
          alignItems: "center",
          display: "flex",
          height: "100%",
          justifyContent: "center",
          left: 0,
          position: "absolute",
          top: 0,
        }}
      >
        <div
          style={{
            color: "rgba(0, 0, 0, 0.2)",
            fontSize: `${8 * props.scale}rem`,
            fontWeight: "bold",
            textTransform: "uppercase",
            transform: "rotate(-45deg)",
            userSelect: "none",
          }}
        >
          Portage
        </div>
      </div>
      {props.annotationLayer.children}
      {props.textLayer.children}
    </>
  );

  // this will add icons of highlighter and boundary box in sidebar menu we can add more if needed
  const defaultLayoutPluginInstance = defaultLayoutPlugin({
    sidebarTabs: (defaultTabs) =>
      defaultTabs.concat(
        {
          content: sidebarNotes,
          icon: <MessageIcon />,
          title: "Notes",
        }
        // {
        //   content: boundaryBoxContent,
        //   icon: (
        //     <Icon size={16}>
        //       <path d='M0.500 7.498 L23.500 7.498 L23.500 16.498 L0.500 16.498 Z' />
        //       <path d='M3.5 9.498L3.5 14.498' />
        //     </Icon>
        //   ),
        //   title: "Boundary Box",
        // }
      ),
  });

  //highlight plugin instance
  const highlightPluginInstance = highlightPlugin({
    renderHighlightTarget,
    renderBoundaryBoxTarget,
    renderHighlightContent,
    renderBoundaryBoxContent,
    renderHighlights,
    renderBoundaryBox,
  });

  const { activateTab } = defaultLayoutPluginInstance;
  const jumpToPagePluginInstance = JumptoPage();
  const { jumpToPage } = jumpToPagePluginInstance;
  const { jumpToHighlightArea } = highlightPluginInstance;

  return (
    <div>
      <CustomTabs />
      <div className="main" style={{display:'flex',flexDirection:'row', height:'512px'}}>
        {/* PDF viewer component */}
        <Worker
          workerUrl="https://unpkg.com/pdfjs-dist@2.11.338/build/pdf.worker.min.js"
          className="worker"
        >
          <Viewer
            fileUrl={buffer}
            renderError={() => (
              <Box
                sx={{
                  display: "flex",
                  flexDirection: "column",
                  width: "140px",
                  margin: "30%",
                  justifyContent: "center",
                  alignItems: "center",
                }}
              >
                <CircularProgress />
              </Box>
            )}
            renderLoader={(percentages) => (
              <div style={{ width: "140px" }}>
                <ProgressBar progress={Math.round(percentages)} />
              </div>
            )}
            plugins={[
              defaultLayoutPluginInstance,
              highlightPluginInstance,
              jumpToPagePluginInstance,
              pageNavigationPluginInstance,
              customCanvasPluginInstance,
              customZoomPluginInstance,
            ]}
            renderPage={renderPage}
            onDocumentLoad={handleDocumentLoad}
            defaultScale={scale}
            onPageChange={onPageChange}
          />
        </Worker>
        {/* end of pdf viewer component */}

        {/* right sidebar components */}
        <div className="rightSide" ref={ref}>
          {show ? (
            <>
              {/* Filters Chips */}
              <DataChip
                chipName={chipName}
                chipSelectionData={chipSelectionData}
              />

              {/* search bar */}
              <SearchFiled
                handleChange={handleChange}
                handleKeyDown={handleKeyDown}
              />

              {/* showing the All the data and searched data */}
              <div className="dataColumn">
                {filterData && filterData.length > 0 ? (
                  <>
                    <>
                      {filterData?.map((val, index) => (
                        <>
                          {/* Data Columns */}
                          <DataColumn
                            val={val}
                            index={index}
                            showText={showText}
                            setShowText={setShowText}
                            sidebarClick={sidebarClick}
                            scale={scale}
                            addScale={addScale}
                            setAddScale={setAddScale}
                            zoomTo={zoomTo}
                            jumpToHighlightArea={jumpToHighlightArea}
                          />

                          {/* textArea for every data column */}
                          <TextAreaBox
                            showText={showText}
                            index={index}
                            textAreaData={textAreaData}
                            handleTextArea={handleTextArea}
                            textareaRef={textareaRef}
                          />
                        </>
                      ))}
                    </>
                    {/* Per Page note section */}
                    <PerPageNote
                      width={width}
                      noteActive={noteActive}
                      setNoteActive={setNoteActive}
                      textareaRef2={textareaRef2}
                      currentPage={currentPage}
                    />
                  </>
                ) : (
                  <div className="NoData">
                    <h4>No Data to show</h4>
                  </div>
                )}
              </div>
            </>
          ) : (
            <div>
              <h3>PDF file is loading.....</h3>
            </div>
          )}
        </div>
      </div>
    </div>
  );
}
