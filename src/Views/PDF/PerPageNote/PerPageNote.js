import React from "react";
import './PerPageNote.css';


const PerPageNote = ({width,noteActive,setNoteActive,textareaRef2,currentPage}) => {
    
    const noteActiveFunction=(value)=>{
        setNoteActive(value)
    }

  return (
    <div className="perPageNote" style={{ width: `${Math.floor(width)}px` }}>
      <div className="twoSections">
        <div
          className="mainNote"
          style={{ backgroundColor: noteActive ? "rgb(235, 232, 232)" : "" }}
          onClick={() => noteActiveFunction(true)}
        >
          Notes
        </div>
        <div
          className="actions"
          style={{ backgroundColor: noteActive ? "" : "rgb(235, 232, 232)" }}
          onClick={() => noteActiveFunction(false)}
        >
          Actions
        </div>
      </div>
      <div className="noteInput">
        {noteActive ? (
          <div style={{ width: "100%" }}>
            <textarea
              ref={textareaRef2}
              className="mainTextArea"
              placeholder={`Write Notes for ${currentPage}`}
              rows={7}
              style={{ width: "98%" }}
            />
          </div>
        ) : (
          <div>TBD Section</div>
        )}
      </div>
    </div>
  );
};

export default PerPageNote;
