import React from 'react'
import './TextAreaBox.css';


const TextAreaBox = ({showText,index,textAreaData,handleTextArea,textareaRef}) => {
    

  return (
    <div className="textAreaBox">
        {showText !== null && showText === index ? (
            <textarea
                className='textArea'
                placeholder="Write a note here.."
                rows="5"
                value={textAreaData}
                style={{width:'100%'}}
                onChange={handleTextArea}
                ref={textareaRef}
            />
            ) : (
            <></> 
        )}
    </div>
  )
}

export default TextAreaBox