import React from "react";
import { Button } from "@mui/material";
import { DataGrid } from "@mui/x-data-grid";
import CustomTabs from '../CustomTabs/CustomTabs';
import { useNavigate } from "react-router-dom";

export default function DataGrids(props) {
  let navigate = useNavigate();

  const [csvArray] = React.useState([
    {
      id: 612,
      date: "10/01/2005",
      pageNo: 86,
      recordtype: "Chiro report",
      facility: "YO D.C.",
      anatomy: "Neck, back, shoulders",
      medicalCondition:
        "Injury, motor vehicle, accident, head-on collision, headache, dizziness, nervous, scared, disoriented, shaky, accident, pain (neck, mid back, low back, shoulders), headache",
      testTreatment: "",
      medication: "",
    },
    {
      id: 712,
      date: "10/01/2005",
      pageNo: 92,
      recordtype: "Progress report",
      facility: "YO D.C.",
      anatomy: "",
      medicalCondition: "",
      testTreatment: "Themapy",
      medication: "",
    },
    {
      id: 754,
      date: "10/01/2005",
      pageNo: 94,
      recordtype: "History and physical",
      facility: "YO D.C.",
      anatomy: "Head, back, mid back, neck, shoulders",
      medicalCondition:
        "Injury, automobile accident, acceleration, pain (back, mid back, both shoulders), other symptoms, stillness, headache, anxiety",
      testTreatment: "",
      medication: "",
    },
    {
      id: 1147,
      date: "10/01/2005",
      pageNo: 150,
      recordtype: "Billing",
      facility: "YO D.C.",
      anatomy: "",
      medicalCondition: "",
      testTreatment: "Mechanical traction",
      medication: "",
    },
    {
      id: 715,
      date: "13/01/2005",
      pageNo: 92,
      recordtype: "Progress report",
      facility: "YO D.C.",
      anatomy: "back",
      medicalCondition: "",
      testTreatment: "",
      medication: "",
    },
    {
      id: 718,
      date: "14/01/2005",
      pageNo: 92,
      recordtype: "Progress report",
      facility: "YO D.C.",
      anatomy: "All over, back",
      medicalCondition: "pain (all over, back)",
      testTreatment: "",
      medication: "",
    },
    {
      id: 1228,
      date: "15/01/2005",
      pageNo: 157,
      recordtype: "Consultation",
      facility: "V M.D. Pain Management",
      anatomy: "",
      medicalCondition: "injuries",
      testTreatment: "",
      medication: "",
    },
    {
      id: 1244,
      date: "15/01/2005",
      pageNo: 158,
      recordtype: "Consultation",
      facility: "V M.D. Pain Management",
      anatomy: "Back, neck, brain, shoulders",
      medicalCondition: "Accident, hurts (back, neck), pain, stress, illnesses",
      testTreatment: "MRIs",
      medication: "",
    },
    {
      id: 1327,
      date: "15/01/2005",
      pageNo: 160,
      recordtype: "Consultation",
      facility: "V M.D. Pain Management",
      anatomy:
        "Neck, shoulder, interscapular region, chin, chest, cervidal spine",
      medicalCondition:
        "Stiffness of posture and movement (neck, shoulder), cremors, scars (neck), deformities, tenderness 1+/1+, spasms, trigger points 1+/1+ (I, axial), tenderness (shoulders), pain (cervical spine), pain (shoulders)",
      testTreatment:
        "Physical examination, height (5'7\"), weighs (219, pounds), range of motion (degrees, right, left, normal), chin to chest (full), Eill extension (20,20), Tilt (20, 20, 20), rotation (60,60,60), shoulder flexion (170,170,170), abuction (170,170,170), extension (60,60,60), internal rotation (80,80,80), external rotation (80,80,80,80,68)",
      medication: "",
    },
    {
      id: 723,
      date: "17/01/2005",
      pageNo: 92,
      recordtype: "Progress report",
      facility: "YO D.C.",
      anatomy: "Boulders, neck, muscles",
      medicalCondition: "Caracy, headeness, stiffness",
      testTreatment: "",
      medication: "",
    },
    {
      id: 729,
      date: "21/01/2005",
      pageNo: 92,
      recordtype: "Progress report",
      facility: "YO D.C.",
      anatomy: "",
      medicalCondition: "walkerphan, andlmers",
      testTreatment: "Headaght, MK mentas",
      medication: "Lende E, MK mentas",
    },
    {
      id: 735,
      date: "24/01/2005",
      pageNo: 92,
      recordtype: "Progress report",
      facility: "YO D.C.",
      anatomy: "boulders, neck",
      medicalCondition: "",
      testTreatment: "",
      medication: "",
    },
    {
      id: 743,
      date: "28/01/2005",
      pageNo: 92,
      recordtype: "Progress report",
      facility: "YO D.C.",
      anatomy: "Muscles",
      medicalCondition: "headache, spasm",
      testTreatment: "PTT",
      medication: "",
    },
    {
      id: 1154,
      date: "01/02/2005",
      pageNo: 150,
      recordtype: "Billing",
      facility: "YO D.C.",
      anatomy: "",
      medicalCondition: "",
      testTreatment:
        "Ultrasound therapy, massage therapy, chiropractic manipulative, wbit, wt (lbs)",
      medication: "",
    },
    {
      id: 597,
      date: "07/02/2005",
      pageNo: 86,
      recordtype: "Chiro report",
      facility: "YO D.C.",
      anatomy: "",
      medicalCondition: "injured, automobile accident",
      testTreatment: "",
      medication: "",
    },
    {
      id: 677,
      date: "07/02/2005",
      pageNo: 91,
      recordtype: "Chiro report",
      facility: "YO D.C.",
      anatomy:
        "back, neck, shoulders, paravertebral, muscles, cervical spine, lumbar spine, trapezius muscles, thoracic spine, sprain/strain, shoulder, soft tissue",
      medicalCondition:
        "v, headache (mid, back, low, back, both), shoulders pain (neck, mid, back, low, back, both), tenderness (bilateral, paravertebral, muscles, cervical spine), tenderness (lumbar, right, left, trapezius muscles), cephalgia, cervical sprain/strain, thoracic spine sprain/strain (cervical spine, lumbar spine, sprain/strain), sprain/strain (lumbar spine, right), shoulder sprain/strain (lumbar spine, sprain/strain, right, left), soft tissue injuries",
      testTreatment:
        "Ultrasound, physical therapy, mechanical traction, electrical stimulation, chiropractic treatment, active therapy",
      medication: "",
    },
    {
      id: 109,
      date: "11/02/2005",
      pageNo: 42,
      recordtype: "ER Report",
      facility: "XYZ emergency center",
      anatomy:
        "Neck, back, chest wall, musculosketal, cardiac, gi, general appearance, respiratory, heart, head/neck, ears, nose, cervical, thoracic, lumbar, aspects of the muscles, trapezius, lattisimus dorsi, shoulder, abdomen, bowels, extremities, neurologic, skin, mediastinal structures, thoracic aorta",
      medicalCondition:
        "Motor vehicle accident, chest wall pain (neck, back), feels nauseus, airbag deployment, shortness of breath, heck and back pain (musculoskeletal), chest wall pain (cardiac), nausea (gi), well-developed (GI, general appearance), well-nourished (GI, general appearance), distress (general appearance, acute, respiratory), normocephalic (head/neck), atraumatic (head/neck), deformity (head/neck, ears, nose), hemotympanum, neck is supple (neck), bony tenderness (musculoskeletal, cervical, thoracic, lumbar), tenderness (chest wall, shoulder), clear (chest), crepitus (chest), regular (heart), soft (abdoment), good bowel sounds (abdomen), nontender (abdoment, bowel), clubbing (extremities), cyanosis (extremeties), edema (extremities), deformity (extremities), alert (neurologic), appropriate (neurologic), warm (neurologic, skin), dry (neurologic, skin), gait is good (neurologic, skin), mediastinal structures are normal (chest, mediastinal structures), hilar adenopathy, mass, nodule, pleural effusion",
      testTreatment:
        "Physical examination, vital signs, blood pressure (147/82), heart rate (88), respirations (16), temperature (98.1), ekg (normal sinus rhythm), rate (71), P-R (interval of 160 millisecons, QRS duration is 94 milliseconds), EKG (normal), CT (mediastinal structures are normal, no mediastinal nor hilar adenopathy, thoracic aorta is normal, no mass, nodule or pleural effusion",
      medication: "",
    },
    {
      id: 214,
      date: "11/02/2005",
      pageNo: 43,
      recordtype: "ER Report",
      facility: "XYZ emergency center",
      anatomy: "Chest wall, muscle",
      medicalCondition:
        "Feeling well, pain, chest wall pain, muscle spasm, motor vehicle accident, condition: good",
      testTreatment:
        "White count (7.5), hematocrit (47.6), platelets (280,000), sodium (138), potassium (3.8), chloride (100), bicarb (30), BUN (16), creatinine (1), glucose (95), calcium (10.1), pulse ox (adequate, 99, %)",
      medication: "Zofran, morphine",
    },
    {
      id: 251,
      date: "11/02/2005",
      pageNo: 47,
      recordtype: "Lab report",
      facility: "XYZ emergency center",
      anatomy: "",
      medicalCondition: "",
      testTreatment: "",
      medication: "",
    },
    {
      id: 252,
      date: "11/02/2005",
      pageNo: "",
      recordtype: "",
      facility: "",
      anatomy: "",
      medicalCondition: "",
      testTreatment:
        "Prothrombin time, PT, seconds (12.8), PT INR (1.05, 0.77-1.26), APTT (33, 24-34), seconds (33)",
      medication: "",
    },
    {
      id: 266,
      date: "11/02/2005",
      pageNo: 48,
      recordtype: "Lab report",
      facility: "XYZ emergency center",
      anatomy: "Back",
      medicalCondition: "Back pain, MVA",
      testTreatment:
        "Alkaline phos (200-495,105-420,U/L,14-15,130-525,70-230,U/L,16-19,65-260)",
      medication: "",
    },
    {
      id: 281,
      date: "11/02/2005",
      pageNo: 49,
      recordtype: "Lab report",
      facility: "XYZ emergency center",
      anatomy: "back",
      medicalCondition: "back pain, MVA",
      testTreatment: "",
      medication: "",
    },
    {
      id: 324,
      date: "11/02/2005",
      pageNo: 50,
      recordtype: "Lab report",
      facility: "XYZ emergency center",
      anatomy: "back",
      medicalCondition: "back pain, MVA (back)",
      testTreatment: "",
      medication: "",
    },
    {
      id: 369,
      date: "11/02/2005",
      pageNo: 68,
      recordtype: "CT scan report",
      facility: "XYZ emergency center",
      anatomy:
        "Bones, soft tissues, heart, great vessels, thoracic aortic, pulmonary arteries, chest",
      medicalCondition:
        "Adenopathy (heart, great vessels), aneurysm, pulmonary arteries are normal, pulmonary embolus (pulmonary arteries), pulmonary infiltrates, masses",
      testTreatment: "CT angiogram (unremarkable), F2, PgDn, F6 (Prev Scrn F7)",
      medication: "",
    },
    {
      id: 430,
      date: "11/02/2005",
      pageNo: 81,
      recordtype: "Collision narrative",
      facility: "XYZ emergency center",
      anatomy: "shoulder, rear bumper, rear tail, leg, face, fenders, shoulder",
      medicalCondition:
        "visual obstructions, damage, traffic collision (right side), defects, collision, fender (left, forward, left side), scraped (left, forward), collision damage, mechanical defects, side fender (right side), dented (right side), injuries (leg, face), prior collision, vehicle 4, GMC, damage, collision, windshield (front, both, fenders), fenders (front, both), undercarriage damage, hit by a red car, vehicle in an attempt, vehicle colliding",
      testTreatment: "US, physical evidence (18)",
      medication: "",
    },
    {
      id: 886,
      date: "11/02/2005",
      pageNo: 135,
      recordtype: "Examination",
      facility: "??",
      anatomy:
        "Cervical spine, thoracic spine, lumbar spine, shoulders, arm, chest, piriformis",
      medicalCondition:
        "Cephalgia, cervical spine sprain/strain, thoracic spine sprain/strain, lumbar spine sprain/strain, sprain/strain (both, shoulders, left, upper, arm), chest contusion (anterior), piriformis syndrome, anxiety",
      testTreatment:
        "Cervical sprain/strain (847.0,3), thoracic sprain/strain (847.1, 4), lumbar spine sprain/strain (847.2, 5), both shoulders sprain/strain (840.9,6), left upper arm sprain/strain (841.9,7,anterior chest contusion, 922.1,8), piriformis syndrome (922.32,9), anxiety (308), ultrasound, physical therapy, mechanical traction, massage, chiropractic treatment",
      medication: "",
    },
    {
      id: 1292,
      date: "11/02/2005",
      pageNo: 159,
      recordtype: "Consultation",
      facility: "V M.D. Pain Management",
      anatomy: "Neck, back, chest wall, sinus, chest, muscle",
      medicalCondition:
        "Motor vehicle accident, vehicle was hit, neck pain, back pain, chest wall pain, normal sinus rhythm, muscle spasm",
      testTreatment:
        "Physical examination, EKG (normal sinus rhytm), CAT scan (normal structures)",
      medication: "",
    },
    {
      id: 230,
      date: "12/02/2005",
      pageNo: 46,
      recordtype: "Discharge instructions",
      facility: "XYZ emergency center",
      anatomy: "Chest wall, back",
      medicalCondition: "Chest wall strain, MVA, back pain",
      testTreatment: "CBC, chemistry, U/S",
      medication: "",
    },
    {
      id: 260,
      date: "12/02/2005",
      pageNo: 48,
      recordtype: "Lab report",
      facility: "XYZ emergency center",
      anatomy: "",
      medicalCondition: "",
      testTreatment: "CHEM (2-Jan)",
      medication: "",
    },
    {
      id: 311,
      date: "12/02/2005",
      pageNo: 49,
      recordtype: "Lab report",
      facility: "XYZ emergency center",
      anatomy: "",
      medicalCondition: "",
      testTreatment: "",
      medication: "",
    },
    {
      id: 288,
      date: "12/02/2005",
      pageNo: "",
      recordtype: "",
      facility: "",
      anatomy: "",
      medicalCondition: "MVA",
      testTreatment:
        "Potassium (3.8,3.5-5.1), mmol/L (3.8, 3.5-5.1), Chloride (100, 98-107,C02), mmo1 (L,100,98-107,C02), mmol/L (30,23-32), Anion gap (8), mmol/L (8), Glucose (mg/dl,95,70-105), BUN (mg/dl,16,8-21), Creatinine (mg/dl, 1.0,0.7-1.2), BUN/CREAT Ratio (60), Alkaline Phos, U/L (34,L,45-122), AST (SGOT) (23,10-34), ALt (SGPT)(44), U/L (44,H), BILI. TOTAL (mg/dl,0.4,0.2-1.0), Total Protein (g/dl,8.4, H, 6.0-8.3), Albumin (g/dl,4.6,3.5-5.0), Globulin (g/dl,3.8), A/G Ratio (1.22), Calcium (mg/dl,10.2,H,8.4-10.2), Alkaline Phos",
      medication: "",
    },
    {
      id: 342,
      date: "12/02/2005",
      pageNo: 50,
      recordtype: "Lab report",
      facility: "XYZ emergency center",
      anatomy: "Hip, heart",
      medicalCondition:
        "MVA, Venous thrombosis, recurrent thrombosis, arterial thrombo, embolism, MI",
      testTreatment:
        "Surgery, hip surgery, PT (12.8,11.1-13.9), seconds (12.8,11.1-13.9), PT INR (1.05), APTT (33,24-34), seconds (33), INR, mechanical heart valves (3.0-4.5,3.5)",
      medication: "",
    },
    {
      id: 367,
      date: "13/02/2005",
      pageNo: 68,
      recordtype: "CT scan report",
      facility: "XYZ emergency center",
      anatomy: "",
      medicalCondition: "",
      testTreatment:
        "CTA CHST (bones and soft tissues are normal in appearance,heart and great vessels are normal,No hilar or mediastinal)",
      medication: "",
    },
    {
      id: 5,
      date: "18/03/2005",
      pageNo: 1,
      recordtype: "MRI Report",
      facility: "M. IMAGING",
      anatomy:
        "Brain, T1, cortical sulci, gyri, cerebral, cerebellar hemispheres, cervocranial junction, supraseller cistern, quadrigeminal plate cistern, maximillary antrum, mucous membrane, frontal sinuses, maxillary, antrum, sides, sinus, cervical spine, T1, vertebral segments, bone marrow, bony structures, cervicocranial junction, cervical cord, c2-c3, disk, c3-c4, disk, thecal sac,, neural foramina, c4-c5, disck, c5-c5, disck, cervical spine, c6-c7, disk, c3-c4, thecal sac",
      medicalCondition:
        "Mass effect (midline), subdural collections, cortical sulci and gyri appear to be normal, abnormal signal (cerebral, cerebeller hemispheres), cerebeller hemispheres (cerebral), abnormalities (cervicocranial junction), suprasellar cistern or quadrigeminal plate (suprasellar cistern, quadrigeminal plate cistern), polyp, retention cyst (lef, maximillary antrium, mucous membrane, frontal sinuses, right, maxillary), mucous membrane (left, maxillary antrum, frontal sinuses, right, maxillary), inflammatory debris, sinus disease, Mi, compression fractures, destructive changes, normal signal intensities (bone marrow, bony structures), abnormalities (cervicocranial junction), focal enlargements (cervical cord), abnormal signal (cervical cord), disk protrusion (c2-c3, posterior), central or lateral stenosis (c2-c3, central), disk protrusion (c3-c4,central, posterior, anterior, thecal sac), neural foramina appear to be normal, disk protrusion (c3-C4, posterior), central or lateral stenosis (c4-c5, central), disk protrusion (c5-c6, posterior), central or lateral stenosis (c5-c6, central), disk protrusion (c6-c7,posterior), central or lateral stenosis (central, lateral), disk protrusion (central, posterior, c3-c4,anterior, thecal sac",
      testTreatment:
        "Imaging, MRI, study, AIRIS II magnetic resonance imaging, magnetic resonance imaging (unremarkable, sinus disease), imaging, MRI brain, imaging, mri, study, AIRIS II magnetic resonance imaging, transaxial gradient echo sequence (vertebral segments demonstrate normal alignment, no compression fractures), bone marrow (within visualized bony structures,relatively normal signal intensities,no abnormalities at cervicocranial junction, cervical cord does not show any), imaging, mri",
      medication: "",
    },
  ]);

  const [columns] = React.useState([
    {
      field: "date",
      headerName: "Date",
      width: 200,
    },
    {
      field: "recordtype",
      headerName: "Record type",
      width: 200,
      renderCell: (params) => (
        <a
          href='#'
          onClick={() =>
            navigate("/pdf-viewer", { state: { page: params.row.pageNo } })
          }
        >
          {params.value}
        </a>
      ),
    },
    {
      field: "facility",
      headerName: "Facility",
      width: 200,
    },
    {
      field: "anatomy",
      headerName: "Anatomy",
      width: 200,
    },
    {
      field: "medicalCondition",
      headerName: "MEDICAL CONDITION",
      width: 200,
    },
    {
      field: "testTreatment",
      headerName: "Test Treatment Procedure",
      width: 200,
    },
    {
      field: "medication",
      headerName: "Medication",
      width: 200,
    },
  ]);

  if (csvArray.length)
    return (
      <>
        <CustomTabs />
        <div style={{ height: 650, width: "100%" }}>
          <DataGrid
            rows={csvArray}
            columns={columns}
            pageSize={20}
            rowsPerPageOptions={[10]}
            onCellClick={(params, event) =>
              navigate("/pdf", { state: { page: params.row.pageNo } })
            }
            checkboxSelection
            disableSelectionOnClick
          />
        </div>
      </>
    );
  else
    return (
      <form id='csv-form'>
        <input type='file' accept='.csv' id='csvFile'></input>
        <br />
        <Button>Upload</Button>
      </form>
    );
}
