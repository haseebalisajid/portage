import React,{useState} from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import Grid from '@mui/material/Grid';
import Typography from '@mui/material/Typography';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import { makeStyles, useTheme } from "@material-ui/core";
import { DataGridPro } from '@mui/x-data-grid-pro';
import {entityData} from '../Data/EnitityData'
import TableModal from '../TableModal/TableModal';

function DetailPanelContent({ row: rowProp }) {
  return (
    <Stack sx={{ py: 2, height: 1, boxSizing: 'border-box' }} direction="column">
      <Paper sx={{ flex: 1,ml:3, width: '30%', p: 2 }}>
        <Stack direction="column"  sx={{ height: 1 }}>
          <DataGridPro
              sx={{
                "flex":1,
                "& .MuiDataGrid-columnHeaders-ioKnUe": {
                  backgroundColor: "rgb(122, 163, 93)",
                  color:'white'
                }
              }}
            columns={[
              { field: 'date', headerName: 'date',width: 200 },
              {
                field: 'customerId',
                headerName: 'customerId',
                type: 'number',
                width: 200
              },
              { field: 'amount', headerName: 'amount', type: 'number',width: 200 },
            ]}
            rows={rowProp.history}
            hideFooter
          />
        </Stack>
      </Paper>
    </Stack>
  );
}

// const columns = [
//   { field: 'id', headerName: 'Order ID' },
//   { field: 'customer', headerName: 'Customer', width: 200 },
//   { field: 'date', type: 'date', headerName: 'Placed at' },
//   { field: 'currency', headerName: 'Currency' },
//   {
//     field: 'total',
//     type: 'number',
//     headerName: 'Total',
//     valueGetter: ({ row }) => {
//       const subtotal = row.products.reduce(
//         (acc, product) => product.unitPrice * product.quantity,
//         0,
//       );

//       const taxes = subtotal * 0.05;
//       return subtotal + taxes;
//     },
//   },
// ];
const columns = [
    {
      field: "date",
      headerName: "Date",
      width: 200,
    },
    {
      field: "recordtype",
      headerName: "Record Type",
      width: 200,
    },
    {
      field: "facility",
      headerName: "Facility",
      width: 200,
    },
    {
      field: "&",
      headerName: "&",
      width: 200,
    },
    {
      field: "professional",
      headerName: "Professional",
      width: 200,
    },
    {
      field: "category",
      headerName: "Category",
      width: 250,
    },
    {
      field: "traits",
      headerName: "Traits",
      width: 200,
    },
    {
      field: "relationshipType",
      headerName: "Relationship Type",
      width: 200,
    },
    {
      field: "relationshipCategory",
      headerName: "Relationship Category",
      width: 200,
    },
    {
      field: "relationshipText",
      headerName: "Relationship Text",
      width: 200,
    },
    {
      field: "Type",
      headerName: "Type",
      width: 200,
    },
    {
      field: "Text",
      headerName: "Text",
      width: 200,
    },
];


const useStyles = makeStyles((theme) => ({
  root: {
    "& .MuiDataGrid-columnHeaders-ioKnUe": {
      backgroundColor: "rgb(122, 163, 93)",
      color:'white'
    }
  },
  Table: {
    height: 500,
  },
  "@media (min-width: 1024px)": {
    Table: {
      height: 430,
    },
  },
  "@media (min-width: 1366px)": {
    Table: {
      height: 530,
    },
  },
  "@media (min-width: 1440px)": {
    Table: {
      height: 700,
    },
  },
  "@media (min-width: 1680px)": {
    Table: {
      height: 790,
    },
  },
  "@media (min-width: 2560px)": {
    Table: {
      height: 920,
    },
  },

}));

const BasicDetailPanels=()=>{
  const classes = useStyles();
    const [page,setPage]=useState(0);
    const [pageSize,setPageSize]=useState(10)
    const [on, setOn] = useState(false);
    const handleOpen = () => setOn(true);
    const handleClose = () => setOn(false);


  const handleCellClick=(params)=>{
    console.log(params)
    if(params.field==='&'){
      handleOpen()
    }
  }

  const getDetailPanelContent = React.useCallback(
    ({ row }) => <DetailPanelContent row={row} />,
    [],
  );

  const getDetailPanelHeight = React.useCallback(() => 400, []);

  return (
    <div className={classes.Table}>
      <TableModal open={on} handleClose={handleClose} />
      <DataGridPro
        columns={columns}
        rows={entityData}
        pagination={true}
        page={page}
        pageSize={pageSize}
        rowsPerPageOptions={[10,25,50]}
        onPageChange={(page)=>setPage(page)}
        onPageSizeChange={(page)=>setPageSize(page)}
        getDetailPanelHeight={getDetailPanelHeight}
        getDetailPanelContent={getDetailPanelContent}
        onCellClick={handleCellClick}
      />
    </div>
  );
}


export default BasicDetailPanels;