import React, { useState } from "react";
import PropTypes from "prop-types";
import { useTheme } from "@mui/material/styles";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import { createStyles, withStyles } from "@material-ui/core";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Box from "@mui/material/Box";
import TablePagination from "@mui/material/TablePagination";


import Row from "../Rows/Rows";
import { entityData } from "../Data/EnitityData";
import Head from "../Head/Head";


const styles = createStyles({
  TableBorder:{
    "& .MuiTableCell-root": {
      borderLeft: "1px solid rgba(224, 224, 224, 1)"
    },
  },
  Table: {
    height: "450px",
  },
  "@media (min-width: 1024px)": {
    Table: {
      height: "350px",
    },
  },
  "@media (min-width: 1366px)": {
    Table: {
      height: "450px",
    },
  },
  "@media (min-width: 1680px)": {
    Table: {
      height: "700px",
    },
  },
  "@media (min-width: 2560px)": {
    Table: {
      height: "920px",
    },
  },

});

const MainTable = withStyles(styles)(({ classes }) => {
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [data,setData]=useState(entityData);
  // const [sort,setSort]=useState(false);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };


  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - data.length) : 0;

  return (
    <Paper sx={{ width: "100%", overflow: "hidden" }}>
      <TableContainer className={classes.Table}>
        <Table stickyHeader className={classes.TableBorder} aria-label="custom pagination table">
          <TableHead>
            <Head/>
          </TableHead>
          <TableBody>
            {(rowsPerPage > 0
              ? data.slice(
                  page * rowsPerPage,
                  page * rowsPerPage + rowsPerPage
                )
              : data
            ).map((row) => (
              <Row key={row.date} row={row}  />
            ))}

            {emptyRows > 0 && (
              <TableRow style={{ height: 53 * emptyRows }}>
                <TableCell colSpan={6} />
              </TableRow>
            )}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[5, 10, 25, { label: "All", value: -1 }]}
        colSpan={3}
        count={data.length}
        rowsPerPage={rowsPerPage}
        page={page}
        SelectProps={{
          inputProps: {
            "aria-label": "rows per page",
          },
          native: true,
        }}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
        // ActionsComponent={TablePaginationActions}
      />
    </Paper>
  );
});

export default MainTable;
 