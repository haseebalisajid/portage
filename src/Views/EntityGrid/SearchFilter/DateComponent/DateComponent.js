import * as React from 'react';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';

export default function DateComponent({value,handleDate,label}) {
  return (
    <LocalizationProvider dateAdapter={AdapterDateFns} >
      <DatePicker
        label={label}
        value={value}
        onChange={handleDate}
        renderInput={(params) => <TextField {...params} sx={{width:'200px !important',marginRight:'5px'}}/>}
      />
    </LocalizationProvider>
  );
}
