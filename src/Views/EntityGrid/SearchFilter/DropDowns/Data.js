const Doc=[
    {label:"Select All",type:'Doc Types'},
    {label:"Diagnostic",type:'Doc Types'},
    {label:"Laboratory",type:'Doc Types'},
    {label:"Radiology",type:'Doc Types'},
    {label:"Patient",type:'Doc Types'},
    {label:"Provided",type:'Doc Types'},
    {label:"Reports",type:'Doc Types'},
    {label:"Orders",type:'Doc Types'},
    {label:"Notes",type:'Doc Types'},
    {label:"Admission",type:'Doc Types'},
    {label:"Billing",type:'Doc Types'},
]
  
const BodyParts=[
    {label:"Select All",type:'BodyParts'},
    {label:"Cervical Spine",type:'BodyParts'},
    {label:"Lumbar Spine",type:'BodyParts'},
    {label:"Shoulder",type:'BodyParts'},
    {label:"Head",type:'BodyParts'},
]

  
const dateIncident=[
    {label:"Select All",type:'DateTime'},
    {label:"Pre-Incident",type:'DateTime'},
    {label:"Date of Incident",type:'DateTime'},
    {label:"post-incident",type:'DateTime'},
]

const Diagnoses=[
    {label:"Select All",type:'Diagnoses'},
    {label:"Sprain Cervical",type:'Diagnoses'},
    {label:"Spine",type:'Diagnoses'},
    {label:"Left Shoulder strain sciatica",type:'Diagnoses'},
]

// const Attributes=[
//     {label:"Select All",type:'Attributes'},
//     {label:"Non-negated",type:'Attributes'},
//     {label:"Medical",type:'Attributes'},
//     {label:"Left Shoulder strain sciatica",type:'Attributes'},
//     {label:"Left Shoulder strain sciatica",type:'Attributes'},
//     {label:"Left Shoulder strain sciatica",type:'Attributes'},
//     {label:"Left Shoulder strain sciatica",type:'Attributes'},
//     {label:"Left Shoulder strain sciatica",type:'Attributes'},
//     {label:"Left Shoulder strain sciatica",type:'Attributes'},
// ]


module.exports={Doc,Diagnoses,BodyParts,dateIncident}