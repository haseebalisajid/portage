import React from "react";
import OutlinedInput from '@mui/material/OutlinedInput';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import ListItemText from '@mui/material/ListItemText';
import Select from '@mui/material/Select';
import Checkbox from '@mui/material/Checkbox';


const DropDown = ({selectedValue,handleChange,MenuProps,data}) => {
  return (
    <div>
      <FormControl sx={{ m: 1, width: 200 }}>
        <InputLabel >{data[0].type}</InputLabel>
        <Select
          multiple
          
          value={selectedValue}
          onChange={handleChange}
          input={<OutlinedInput label={data[0].type} />}
          // renderValue={(selected) => {
          //     return <em>Placeholder</em>;
          // }}
          MenuProps={MenuProps}
        >
          {data.map((name, index) => (
            <MenuItem key={index} value={name}>
              <Checkbox checked={selectedValue.indexOf(name) > -1} />
              {name.label}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </div>
  );
};

export default DropDown;
