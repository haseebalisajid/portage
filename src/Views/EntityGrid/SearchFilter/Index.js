import React, { useState, useRef } from "react";
import "./index.css";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import Chip from "@mui/material/Chip";
import CancelOutlinedIcon from "@mui/icons-material/CancelOutlined";
import moment from "moment";

// import DatePicker from "react-datepicker";
// import "react-datepicker/dist/react-datepicker.css";

import DropDown from "./DropDowns/DropDown";
import DateComponent from "./DateComponent/DateComponent";

import { Doc, Diagnoses, BodyParts, dateIncident } from "./DropDowns/Data";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 200,
    },
  },
};

const Index = () => {
  const [showIcon, setShowIcon] = useState(true);
  const [personName, setPersonName] = useState([]);
  const [startDate, setStartDate] = useState(null);
  const [endDate, setEndDate] = useState(null);

  const [value, setValue] = useState("");

  // const startDateRef = useRef();
  // const endDateRef = useRef();




  //function for every selection change
  const handleChange = (event) => {
    const {
      target: { value },
    } = event;

    if(value.length>0){
      if(value[value.length-1].label=='Select All'){
        const data=personName.filter((val)=>val.type !==value[value.length-1].type);
        data.push(value[value.length-1])
        setPersonName(data)
      }
      else{
        const index=value.findIndex((val)=>val.label==='Select All' && val.type==value[value.length-1].type);
        if(index>-1){
          value.splice(index,1)
          setPersonName(value);
        }
        else{
          setPersonName(value);
        }
      }
    }
    else{
      setPersonName(value);
    }
  };


  const handleStartDate=(newValue)=>{
    let dateValue=moment(newValue).format('MM/DD/YYYY');
    // dateValue=dateValue.split('-').reverse().join('-');
    if(startDate!==null){
      let data=personName
      const index=data.findIndex((val)=> val.type ==='From');
      let newObj={label:dateValue,type:'From'}
      data[index]=newObj

      setStartDate(newValue)
      setPersonName(data);
    }
    else{
      console.log('in outer else')
      setStartDate(newValue);
      setPersonName([...personName,{label:dateValue,type:'From'}])
    }    
  }

  const handleEndDate=(newValue)=>{
    let dateValue=moment(newValue).format('MM/DD/YYYY');
    console.log(dateValue)
    // dateValue=dateValue.split('-').reverse().join('-');
    if(endDate!==null){
      let data=personName
      const index=data.findIndex((val)=> val.type ==='To');
      let newObj={label:dateValue,type:'To'}
      data[index]=newObj
      console.log('here',data);
      setEndDate(newValue)
      setPersonName(data);
    }
    else{
      console.log('in outer else')
      setEndDate(newValue);
      setPersonName([...personName,{label:dateValue,type:'To'}])
    }    
  }

  const clearAll=()=>{
    setPersonName([]);
    setStartDate(null);
    setEndDate(null);
  }

  //function for creating chip data from searched text
  const handleKeyDown = (event) => {
    if (event.key === "Enter") {
      if (value !== "") {
        setPersonName([...personName, { label: value, type: "Search" }]);
        setValue("");
      }
    }

  };

  const handleDelete = (chip) => {
    setPersonName(personName.filter((val) => val !== chip));
  };
  return (
    <div className="mainDiv">
      <div className="search">
        <div className="icon">
          <img src="https://www.w3schools.com/howto/searchicon.png" />
        </div>
        <div className="chipData">
          <div className="chips">
            {personName.map((val, index) => (
              <Chip
                label={`${val.type}: ${val.label}`}
                // label={val.label}
                onDelete={() => handleDelete(val)}
                sx={{ margin: 0.5 }}
                key={index}
              />
            ))}
            <input
              type="text"
              placeholder="Type to Search"
              className="inputField"
              value={value}
              onChange={(e) => setValue(e.target.value)}
              onKeyDown={handleKeyDown}
            />
          </div>
        </div>
        <div className="leftIcons">
          <div
            className="showIcon2"
            onClick={clearAll}
          >
            {personName.length > 0 ? (
              <CancelOutlinedIcon fontSize="medium" />
            ) : (
              <></>
            )}
          </div>
          <div onClick={() => setShowIcon(!showIcon)} className="showIcon">
            {showIcon ? (
              <KeyboardArrowDownIcon fontSize="large" />
            ) : (
              <KeyboardArrowUpIcon fontSize="large" />
            )}
          </div>
        </div>
      </div>
      {!showIcon ? (
        <div className="filters">
          <DropDown
            selectedValue={personName}
            handleChange={handleChange}
            MenuProps={MenuProps}
            data={Doc}
          />
          <DropDown
            selectedValue={personName}
            handleChange={handleChange}
            MenuProps={MenuProps}
            data={Diagnoses}
          />

          <DropDown
            tag="Doc Types"
            selectedValue={personName}
            handleChange={handleChange}
            MenuProps={MenuProps}
            data={BodyParts}
          />

          <DropDown
            tag="Doc Types"
            selectedValue={personName}
            handleChange={handleChange}
            MenuProps={MenuProps}
            data={dateIncident}
          />
          <DateComponent value={startDate} handleDate={handleStartDate} label='Date From'/>
          <DateComponent value={endDate} handleDate={handleEndDate} label='Date To'/>
        </div>
      ) : (
        <></>
      )}
    </div>
  );
};

export default Index;
