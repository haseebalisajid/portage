import React,{useState} from 'react'
import Box from '@mui/material/Box';
import Collapse from '@mui/material/Collapse';
import IconButton from '@mui/material/IconButton';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@mui/icons-material/KeyboardArrowUp';

import TableModal from '../TableModal/TableModal';

const Rows = (props) => {
  const [on, setOn] = useState(false);
  const handleOpen = () => setOn(true);
  const handleClose = () => setOn(false);

  const { row } = props;
  const [open, setOpen] = React.useState(false);

    return (
        <React.Fragment>
          <TableModal open={on} handleClose={handleClose} />
          <TableRow hover sx={{ '& > *': { borderBottom: 'unset' } }}>
            <TableCell>
              <IconButton
                aria-label="expand row"
                size="small"
                onClick={() => setOpen(!open)}
              >
                {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
              </IconButton>
            </TableCell>
            <TableCell component="th" scope="row">
              {row.date}
            </TableCell>
            <TableCell >{row.recordtype}</TableCell>
            <TableCell sx={{textDecoration:'underline',color:'blue',cursor:'pointer'}}>{row.facility}</TableCell>
            <TableCell >{row.professional}</TableCell>
            <TableCell >{row.category}</TableCell>
            <TableCell sx={{textAlign:'center', textDecoration:'underline',color:"blue",cursor:'pointer'}} onClick={handleOpen}>T</TableCell>
            <TableCell >{row.traits}</TableCell>
            <TableCell >{row.relationshipType}</TableCell>
            <TableCell >{row.relationshipCategory}</TableCell>
            <TableCell >{row.relationshipText}</TableCell>
            <TableCell >{row.Type}</TableCell>
            <TableCell >{row.Text}</TableCell>
          </TableRow>
          <TableRow>
            <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={12}>
              <Collapse in={open} timeout="auto" unmountOnExit>
                <Box sx={{ margin: 2 }}>
                  <Table size="small" aria-label="purchases">
                    <TableHead>
                      <TableRow sx={{color:'white',backgroundColor:'rgb(122, 163, 93)'}}>
                        <TableCell>Date</TableCell>
                        <TableCell>Customer</TableCell>
                        <TableCell >Amount</TableCell>
                        <TableCell >Total price ($)</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {row?.history?.map((historyRow) => (
                        <TableRow key={historyRow.date}>
                          <TableCell component="th" scope="row">
                            {historyRow.date}
                          </TableCell>
                          <TableCell>{historyRow.customerId}</TableCell>
                          <TableCell >{historyRow.amount}</TableCell>
                          <TableCell >
                            {Math.round(historyRow.amount * row.price * 100) / 100}
                          </TableCell>
                        </TableRow>
                      ))}
                    </TableBody>
                  </Table>
                </Box>
              </Collapse>
            </TableCell>
          </TableRow>
        </React.Fragment>
    );
}

export default Rows