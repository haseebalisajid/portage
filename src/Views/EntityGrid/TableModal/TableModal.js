import * as React from 'react';
import Backdrop from '@mui/material/Backdrop';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import { createStyles, withStyles } from "@material-ui/core";

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
  overflow:'hidden'
};


const styles = createStyles({
    TableBorder:{
      "& .MuiTableCell-root": {
        borderLeft: "1px solid rgba(224, 224, 224, 1)"
      },
    },
    Table: {
        height: "400px",
    },
    TableHeader:{
        backgroundColor:'rgb(122, 163, 93)',
        paddingBottom:0,
        paddingTop:0,
        color:'white'
    },
    TableRightRow:{
        width:600,
        paddingBottom:10,
        paddingTop:10,
        color:'rgb(147, 173, 130)'
    },
    TableLeftRow:{
        width:150 ,
        paddingBottom:10,
        paddingTop:10,
        color:'rgb(147, 191, 119)',
        fontWeight:'bold'
    }
  });

const TransitionsModal=withStyles(styles)(({open,handleClose, classes })=> {

    const rowDummyData=[
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
        {
            test:'testing test',
            result:'testing result'
        },
    ]

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
            <Paper sx={style}>
                <TableContainer className={classes.Table}>
                    <Table stickyHeader className={classes.TableBorder} aria-label="modal table">
                        <TableHead>
                            <TableRow  >
                                <TableCell className={classes.TableHeader}>Test</TableCell>
                                <TableCell className={classes.TableHeader}>Result</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {rowDummyData?.map((val)=>(
                                <TableRow>
                                    <TableCell className={classes.TableLeftRow} >{val.test}</TableCell>
                                    <TableCell className={classes.TableRightRow} >{val.result}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
            </Paper>
        </Fade>
      </Modal>
    </div>
  );
});

export default TransitionsModal