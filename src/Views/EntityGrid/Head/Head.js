import React from 'react'
import TableCell from "@mui/material/TableCell";
import TableRow from "@mui/material/TableRow";

const Head = () => {

  let headers=['Expand','Date','Record Type','Facility','Professional','Category','Details','Traits','Relationship Type','Relationship Category','Relationship Text','Type','Text']
  return (
    <TableRow>
      {headers.map((value)=>(
        <TableCell>{value}</TableCell>
      ))}
    </TableRow>
  )
}

export default Head