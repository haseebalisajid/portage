import React from "react";
import CustomTabs from '../CustomTabs/CustomTabs';
import { useNavigate } from "react-router-dom";
import EntityTest from "./MainTable/EntityTest";
import MainTable from "./MainTable/MainTable";
import SearchFilter from './SearchFilter/Index'

export default function EntityGrid() {  
  let navigate = useNavigate();
  
  return (
    <>
      <CustomTabs />
      <SearchFilter />
      <EntityTest />
      {/* <div style={{ height: 600, width: "100%" }}>
        <DataGrid
          rows={csvArray}
          columns={columns}
          pageSize={10}
          rowsPerPageOptions={[10]}
            onCellClick={(params, event) =>
              navigate("/pdf", { state: { page: params.row.pageNo } })
            }
          checkboxSelection
          disableSelectionOnClick
        />
      </div> */}
    </>
  );
}
