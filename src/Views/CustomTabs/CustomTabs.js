import React from "react";
import Tab from "@mui/material/Tab";
import Tabs from "@mui/material/Tabs";
import { useNavigate } from "react-router-dom";
import { useLocation } from "react-router-dom";

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function CustomTabs() {
  let navigate = useNavigate();
  const location = useLocation();

  const handleChange = (event, newValue) => {
    navigate("/" + newValue);
  };

  return (
    <Tabs
      value={location.pathname.replace("/", "")}
      onChange={handleChange}
      aria-label='basic tabs example'
    >
      <Tab alue='' label='List' {...a11yProps(0)} />
      <Tab value='timeline' label='Timeline' {...a11yProps(1)} />
      <Tab value='entity' label='Entity  Grid' {...a11yProps(1)} />
      <Tab value='dashboard' label='Dashboard' {...a11yProps(3)} />
      <Tab value='data-grid' label='Document Grid' {...a11yProps(2)} />
      <Tab value='search' label='Search' {...a11yProps(5)} />
      {/* <Tab value='pdf-viewer' label='Viewer' {...a11yProps(6)} /> */}
      <Tab value='pdf' label='PDF' {...a11yProps()} />
    </Tabs>
  );
}
