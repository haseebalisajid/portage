import React,{useEffect,useRef} from "react";
import CustomTabs from "../CustomTabs/CustomTabs";
import { embedDashboard } from 'amazon-quicksight-embedding-sdk';
import Iframe from 'react-iframe';

export default function Dashboard() {
  const iframeRef=useRef(null);
  useEffect(()=>{
    
    iframeRef.current.src='https://us-east-1.quicksight.aws.amazon.com/embed/53d25f48ba484c3a94f10069771995c1/dashboards/7defcfea-7351-4a75-9069-2763f995a3ea?resetDisabled=true'
  },[])

  return (
    <>
      <CustomTabs />
      <div style={{display:'flex',justifyContent:'center',alignItems:'center'}}>
        <iframe 
        title="Dashboard"
          width="100%"
          ref={iframeRef}
          height="500px"
        />
      </div>
      {/* <div id="embeddingContainer"></div> */}
    </>
  );
}
