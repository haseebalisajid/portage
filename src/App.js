import * as React from "react";
import Pdf from "./Views/PDF/Pdf";
import List from "./Views/Listing/Listing";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Dashboard from "./Views/Dashboard/Dashboard";
import DataGrids from "./Views/DataGrid/DataGrid";
import EntityGrid from "./Views/EntityGrid/EntityGrid";
import Typography from "@mui/material/Typography";
import TimelineInfo from "./Views/TimelineInfo/TimelineInfo";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { createTheme, ThemeProvider } from "@mui/material/styles";
import { LicenseInfo } from '@mui/x-data-grid-pro';

const theme = createTheme({
  palette: {
    primary: { main: "#26580F" }
  },
});
const LICENCESS_KEY=process.env.REACT_APP_LICENCE_KEY;
export default function App() {
  LicenseInfo.setLicenseKey(
    LICENCESS_KEY,
  );
  return (
    <ThemeProvider theme={theme}>
      <AppBar position='static'>
        <Toolbar>
          <Typography variant='h6' component='div' sx={{ flexGrow: 1 }}>
            Medrecs Navigator
          </Typography>
        </Toolbar>
      </AppBar>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<List />} />
          <Route path='/data-grid' element={<DataGrids />} />
          <Route path='/timeline' element={<TimelineInfo />} />
          {/* <Route path='/pdf-viewer' element={<ReactPdfViewer />} /> */}
          <Route path='/entity' element={<EntityGrid />} />
          <Route path='/dashboard' element={<Dashboard />} />
          <Route path='/pdf' element={<Pdf />} />
        </Routes>
      </BrowserRouter>
    </ThemeProvider>
  );
}
