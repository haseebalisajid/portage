export const sampleData = [
  {
      "text": "Mi",
      "width": 0.04387873038649559,
      "height": 0.027414266020059586,
      "left": 0.10803762823343277,
      "top": 0.05490294098854065,
      "pageIndex": 0,
      "type": "default",
  },
  {
      "text": "IMAGING, INC.",
      "width": 0.29237982630729675,
      "height": 0.03166045993566513,
      "left": 0.27567407488822937,
      "top": 0.055890120565891266,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "Patient: Da",
      "width": 0.10057587921619415,
      "height": 0.014268354512751102,
      "left": 0.10444720834493637,
      "top": 0.22256237268447876,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "n",
      "width": 0.00738378195092082,
      "height": 0.009911082684993744,
      "left": 0.30839717388153076,
      "top": 0.22726765275001526,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "DOB:",
      "width": 0.05278008058667183,
      "height": 0.013621090911328793,
      "left": 0.5146436095237732,
      "top": 0.22474460303783417,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "Exam Date: 03/18/2005",
      "width": 0.21000029146671295,
      "height": 0.013951582834124565,
      "left": 0.515426516532898,
      "top": 0.24429188668727875,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "Physician: Dr. Ye",
      "width": 0.15296193957328796,
      "height": 0.015742382034659386,
      "left": 0.10295727849006653,
      "top": 0.28108301758766174,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "MRI OF THE BRAIN",
      "width": 0.20143841207027435,
      "height": 0.013783236034214497,
      "left": 0.10291324555873871,
      "top": 0.3389447033405304,
      "pageIndex": 0,
      "type": "test"
  },
  {
      "text": "The study was performed on an AIRIS II magnetic resonance imaging system.",
      "width": 0.6629892587661743,
      "height": 0.01883184351027012,
      "left": 0.10267813503742218,
      "top": 0.3769610524177551,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "Technique: 1. Axial T2-weighted sequence",
      "width": 0.38314831256866455,
      "height": 0.017792996019124985,
      "left": 0.10229228436946869,
      "top": 0.41556012630462646,
      "pageIndex": 0,
      "type": "procedure"
  },
  {
      "text": "2. Axial FLAIR sequence",
      "width": 0.22011153399944305,
      "height": 0.016940752044320107,
      "left": 0.21900016069412231,
      "top": 0.43553727865219116,
      "pageIndex": 0,
      "type": "procedure"
  },
  {
      "text": "3. Sagittal T1-weighted sequence",
      "width": 0.283522367477417,
      "height": 0.017077913507819176,
      "left": 0.21903729438781738,
      "top": 0.4548301696777344,
      "pageIndex": 0,
      "type": "procedure"
  },
  {
      "text": "4. Coronal T1-weighted sequence",
      "width": 0.2878878712654114,
      "height": 0.017563864588737488,
      "left": 0.2180936634540558,
      "top": 0.47398537397384644,
      "pageIndex": 0,
      "type": "procedure"
  },
  {
      "text": "Findings:",
      "width": 0.08430381864309311,
      "height": 0.016477271914482117,
      "left": 0.1021210253238678,
      "top": 0.512650728225708,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "Ventricular system appears to be normal. There is no mass effect or shift of midline",
      "width": 0.7099391222000122,
      "height": 0.0169471874833107,
      "left": 0.10085678845643997,
      "top": 0.5512000322341919,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "structures. I do not see any subdural collections. Cortical sulci and gyri appear to be",
      "width": 0.7106271982192993,
      "height": 0.01837002858519554,
      "left": 0.1009153202176094,
      "top": 0.5711713433265686,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "normal.",
      "width": 0.06455347687005997,
      "height": 0.013602294959127903,
      "left": 0.10088463127613068,
      "top": 0.5897959470748901,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "There are no areas of abnormal signal intensity within the white or gray matter of cerebral",
      "width": 0.7617875933647156,
      "height": 0.018398437649011612,
      "left": 0.1010558009147644,
      "top": 0.6290154457092285,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "or cerebellar hemispheres. There are no abnormalities at cervicocranial junction,",
      "width": 0.6814946532249451,
      "height": 0.01834733411669731,
      "left": 0.10087316483259201,
      "top": 0.648398220539093,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "suprasellar cistern or quadrigeminal plate cistern.",
      "width": 0.41601666808128357,
      "height": 0.017151489853858948,
      "left": 0.10110621154308319,
      "top": 0.6677433848381042,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "Incidentally, a large polyp or retention cyst is noted within left maxillary antrum with",
      "width": 0.7223629355430603,
      "height": 0.01809731312096119,
      "left": 0.10043508559465408,
      "top": 0.7060936093330383,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "evidence of mucous membrane reaction within the frontal sinuses and right maxillary",
      "width": 0.7240269780158997,
      "height": 0.018615279346704483,
      "left": 0.10057076811790466,
      "top": 0.725426435470581,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "antrum. There is also evidence of inflammatory debris within a number of ethmoid air",
      "width": 0.7286058664321899,
      "height": 0.017176736146211624,
      "left": 0.10089061409235,
      "top": 0.7450590133666992,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "cells on both sides.",
      "width": 0.1599392592906952,
      "height": 0.013315651565790176,
      "left": 0.10012423992156982,
      "top": 0.7644164562225342,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "Conclusion:",
      "width": 0.10685797035694122,
      "height": 0.013593639247119427,
      "left": 0.09976579248905182,
      "top": 0.8033233284950256,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "1.",
      "width": 0.015762068331241608,
      "height": 0.012917092069983482,
      "left": 0.10011826455593109,
      "top": 0.842208206653595,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "Unremarkable Magnetic Resonance Imaging of brain.",
      "width": 0.48875460028648376,
      "height": 0.017938565462827682,
      "left": 0.15728823840618134,
      "top": 0.8422452211380005,
      "pageIndex": 0,
      "type": "diagnosis"
  },
  {
      "text": "2.",
      "width": 0.01645834557712078,
      "height": 0.013123280368745327,
      "left": 0.0990670770406723,
      "top": 0.8612188696861267,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "Sinus disease as described above.",
      "width": 0.2984476685523987,
      "height": 0.013732022605836391,
      "left": 0.15750785171985626,
      "top": 0.861405611038208,
      "pageIndex": 0,
      "type": "diagnosis"
  },
  {
      "text": "000001",
      "width": 0.06585937738418579,
      "height": 0.013345947489142418,
      "left": 0.8743640780448914,
      "top": 0.9384810924530029,
      "pageIndex": 0,
      "type": "default"
  },
  {
      "text": "2",
      "width": 0.015108498744666576,
      "height": 0.016897527500987053,
      "left": 0.8932558298110962,
      "top": 0.9610438346862793,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "M.",
      "width": 0.04171805828809738,
      "height": 0.02871997095644474,
      "left": 0.11027539521455765,
      "top": 0.05782915651798248,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "IMAGING, INC.",
      "width": 0.2955648601055145,
      "height": 0.03762212023139,
      "left": 0.27402621507644653,
      "top": 0.05519464984536171,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "DI",
      "width": 0.017623720690608025,
      "height": 0.01406546775251627,
      "left": 0.10745187103748322,
      "top": 0.2265523076057434,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "n",
      "width": 0.009276313707232475,
      "height": 0.010015093721449375,
      "left": 0.22822554409503937,
      "top": 0.2304106503725052,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "MRI Brain",
      "width": 0.09374711662530899,
      "height": 0.015284703113138676,
      "left": 0.10748563706874847,
      "top": 0.24489569664001465,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "March 18, 2005",
      "width": 0.1345662921667099,
      "height": 0.018533838912844658,
      "left": 0.10779526829719543,
      "top": 0.2631261348724365,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "Page 2 of 2",
      "width": 0.09648267179727554,
      "height": 0.019003869965672493,
      "left": 0.10751611739397049,
      "top": 0.2827661335468292,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "Thank you for referring this patient.",
      "width": 0.30494728684425354,
      "height": 0.023616846650838852,
      "left": 0.1072528213262558,
      "top": 0.37665659189224243,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "Y/",
      "width": 0.02137257345020771,
      "height": 0.013446385972201824,
      "left": 0.18058687448501587,
      "top": 0.5545729994773865,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "1, M.D.",
      "width": 0.06242388114333153,
      "height": 0.01734233647584915,
      "left": 0.2778855860233307,
      "top": 0.5541228652000427,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "Diplomate, American Board of Radiology",
      "width": 0.3588211238384247,
      "height": 0.026447998359799385,
      "left": 0.10664040595293045,
      "top": 0.5688261389732361,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "03/18/2005 (RC)",
      "width": 0.13882602751255035,
      "height": 0.01756839081645012,
      "left": 0.106903575360775,
      "top": 0.6112743616104126,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "000002",
      "width": 0.06717376410961151,
      "height": 0.0155620276927948,
      "left": 0.8741769790649414,
      "top": 0.9369783997535706,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": "3",
      "width": 0.014421369880437851,
      "height": 0.01724489964544773,
      "left": 0.9055863618850708,
      "top": 0.9660590291023254,
      "pageIndex": 1,
      "type": "default"
  },
  {
      "text": ":",
      "width": 0.005908849183470011,
      "height": 0.01098989974707365,
      "left": 0.6401370167732239,
      "top": 0.041727133095264435,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "M.",
      "width": 0.04296364262700081,
      "height": 0.0281760822981596,
      "left": 0.10529634356498718,
      "top": 0.05638715624809265,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "IMAGING, INC.",
      "width": 0.30108609795570374,
      "height": 0.0325871966779232,
      "left": 0.26527100801467896,
      "top": 0.05753008648753166,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "Patient: Da",
      "width": 0.09807666391134262,
      "height": 0.013447695411741734,
      "left": 0.10301265120506287,
      "top": 0.22614786028862,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "DOB:",
      "width": 0.05172209069132805,
      "height": 0.013438470661640167,
      "left": 0.5140184760093689,
      "top": 0.2278236597776413,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "Exam Date: 03/18/2005",
      "width": 0.21019847691059113,
      "height": 0.0143641522154212,
      "left": 0.5140171051025391,
      "top": 0.24735917150974274,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "Physician: Dr. Y",
      "width": 0.15046513080596924,
      "height": 0.016471751034259796,
      "left": 0.10270997136831284,
      "top": 0.2842700183391571,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "MRI OF THE CERVICAL SPINE",
      "width": 0.3144975006580353,
      "height": 0.01435100194066763,
      "left": 0.10178986191749573,
      "top": 0.3426525592803955,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "The study was performed on an AIRIS II magnetic resonance imaging system.",
      "width": 0.6632237434387207,
      "height": 0.01847209595143795,
      "left": 0.1009015217423439,
      "top": 0.38097065687179565,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "Technique:",
      "width": 0.10249625891447067,
      "height": 0.016970381140708923,
      "left": 0.10017004609107971,
      "top": 0.4193876087665558,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "1. Sagittal T1-weighted sequence",
      "width": 0.2820768654346466,
      "height": 0.017756598070263863,
      "left": 0.21963448822498322,
      "top": 0.4200635850429535,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "2. Sagittal T2-weighted sequence",
      "width": 0.28446266055107117,
      "height": 0.017615579068660736,
      "left": 0.21719717979431152,
      "top": 0.43940314650535583,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "3. Transaxial gradient echo sequence",
      "width": 0.31417131423950195,
      "height": 0.01690368726849556,
      "left": 0.21739427745342255,
      "top": 0.4589138627052307,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "Findings:",
      "width": 0.08592381328344345,
      "height": 0.01670055091381073,
      "left": 0.09958425164222717,
      "top": 0.49703219532966614,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "Vertebral segments demonstrate normal alignment. There are no compression fractures or",
      "width": 0.7612206339836121,
      "height": 0.017949163913726807,
      "left": 0.10021962970495224,
      "top": 0.5361663103103638,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "destructive changes. Bone marrow within visualized bony structures demonstrates",
      "width": 0.6962251663208008,
      "height": 0.018155628815293312,
      "left": 0.09961144626140594,
      "top": 0.555194079875946,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "relatively normal signal intensities.",
      "width": 0.2976825535297394,
      "height": 0.0165611132979393,
      "left": 0.09947945922613144,
      "top": 0.5748571753501892,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "There are no abnormalities at cervicocranial junction. Cervical cord does not show any",
      "width": 0.7365584373474121,
      "height": 0.018376853317022324,
      "left": 0.09947993606328964,
      "top": 0.6135491132736206,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "focal enlargements or focal areas of abnormal signal intensity.",
      "width": 0.5259254574775696,
      "height": 0.017845846712589264,
      "left": 0.09955968707799911,
      "top": 0.632743775844574,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "C2-C3: There is no indication of a posterior disk protrusion. There is no evidence of",
      "width": 0.7170267105102539,
      "height": 0.018059803172945976,
      "left": 0.09960523992776871,
      "top": 0.6714162230491638,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "central or lateral stenosis.",
      "width": 0.2162393182516098,
      "height": 0.01348410826176405,
      "left": 0.09951439499855042,
      "top": 0.6914308071136475,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "C3-C4: There is a 1mm central posterior disk protrusion indenting the anterior aspect of",
      "width": 0.7481730580329895,
      "height": 0.01820756308734417,
      "left": 0.09876333177089691,
      "top": 0.7297723293304443,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "the thecal sac. Neural foramina appear to be normal.",
      "width": 0.4418864846229553,
      "height": 0.016707319766283035,
      "left": 0.09852239489555359,
      "top": 0.7495458722114563,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "C4-C5: There is no indication of a posterior disk protrusion. There is no evidence of",
      "width": 0.716911792755127,
      "height": 0.0175874475389719,
      "left": 0.09892633557319641,
      "top": 0.7879481911659241,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "central or lateral stenosis.",
      "width": 0.21563942730426788,
      "height": 0.013452980667352676,
      "left": 0.0985635295510292,
      "top": 0.8077001571655273,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "C5-C6: There is no indication of a posterior disk protrusion. There is no evidence of",
      "width": 0.7171247005462646,
      "height": 0.017844682559370995,
      "left": 0.0983164831995964,
      "top": 0.8463596701622009,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "central or lateral stenosis.",
      "width": 0.21573616564273834,
      "height": 0.013147138990461826,
      "left": 0.09819270670413971,
      "top": 0.8663363456726074,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "000003",
      "width": 0.0662510022521019,
      "height": 0.013613946735858917,
      "left": 0.8742402791976929,
      "top": 0.9383534789085388,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "4",
      "width": 0.01236270647495985,
      "height": 0.016755593940615654,
      "left": 0.9078112840652466,
      "top": 0.9614438414573669,
      "pageIndex": 2,
      "type": "default"
  },
  {
      "text": "Mr",
      "width": 0.04320228099822998,
      "height": 0.02760418690741062,
      "left": 0.10641059279441833,
      "top": 0.05767716467380524,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "IMAGING, INC.",
      "width": 0.2946934700012207,
      "height": 0.031988561153411865,
      "left": 0.2716987431049347,
      "top": 0.0581926591694355,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "Da",
      "width": 0.019337840378284454,
      "height": 0.013287353329360485,
      "left": 0.10362440347671509,
      "top": 0.22614385187625885,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "1",
      "width": 0.005830150097608566,
      "height": 0.00967186689376831,
      "left": 0.22602497041225433,
      "top": 0.23013992607593536,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "MRI Cervical spine",
      "width": 0.1658255159854889,
      "height": 0.01630859449505806,
      "left": 0.1034487709403038,
      "top": 0.24549347162246704,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "March 18, 2005",
      "width": 0.13345183432102203,
      "height": 0.015482288785278797,
      "left": 0.10301774740219116,
      "top": 0.264767587184906,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "Page 2 of 2",
      "width": 0.09486662596464157,
      "height": 0.01636582799255848,
      "left": 0.10311120748519897,
      "top": 0.2839652895927429,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "C6-C7: There is no indication of a posterior disk protrusion. There is no evidence of",
      "width": 0.7152295708656311,
      "height": 0.01800650916993618,
      "left": 0.10405991226434708,
      "top": 0.34139129519462585,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "central or lateral stenosis.",
      "width": 0.21527822315692902,
      "height": 0.01418151892721653,
      "left": 0.1027286946773529,
      "top": 0.36070674657821655,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "Conclusion:",
      "width": 0.10593577474355698,
      "height": 0.013563842512667179,
      "left": 0.10278896242380142,
      "top": 0.3996501564979553,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "There is a 1mm central posterior disk protrusion at C3-C4 level indenting the",
      "width": 0.7000181674957275,
      "height": 0.018736766651272774,
      "left": 0.10187473148107529,
      "top": 0.4384186565876007,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "anterior aspect of the thecal sac.",
      "width": 0.28955909609794617,
      "height": 0.016695361584424973,
      "left": 0.10264983773231506,
      "top": 0.4577188193798065,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "Thank you for referring this patient.",
      "width": 0.30283135175704956,
      "height": 0.016695944592356682,
      "left": 0.10199970006942749,
      "top": 0.49578791856765747,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "D Y",
      "width": 0.03283926472067833,
      "height": 0.013387561775743961,
      "left": 0.1585204303264618,
      "top": 0.650794506072998,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "R, M.D.",
      "width": 0.06636065244674683,
      "height": 0.015284701250493526,
      "left": 0.265407919883728,
      "top": 0.6512791514396667,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "Diplomate, American Board of Radiology",
      "width": 0.35596898198127747,
      "height": 0.017088845372200012,
      "left": 0.10071156173944473,
      "top": 0.6699503064155579,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "03/18/2005 (RC)",
      "width": 0.13670751452445984,
      "height": 0.015307784080505371,
      "left": 0.1006859689950943,
      "top": 0.7091541290283203,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "000004",
      "width": 0.06682358682155609,
      "height": 0.013777077198028564,
      "left": 0.87413489818573,
      "top": 0.9383448362350464,
      "pageIndex": 3,
      "type": "default"
  },
  {
      "text": "5",
      "width": 0.013960965909063816,
      "height": 0.016907403245568275,
      "left": 0.9074583649635315,
      "top": 0.9677514433860779,
      "pageIndex": 3,
      "type": "default"
  }
]

export const highLightData = [
  {
    text: "Mi",
    coordinates: [
      [0.10803762823343277, 0.05490294098854065],
      [0.15191635489463806, 0.05490294098854065],
      [0.15191635489463806, 0.08231720328330994],
      [0.10803762823343277, 0.08231720328330994],
    ],
    highlightAreas: {
      width: 74.5938416570425,
      height: 10000.31138524413109,
      left: 183.6639679968357,
      top: 500.78647017478943,
      pageIndex: 0,
    },
  },
  {
    text: "IMAGING, INC.",
    coordinates: [
      [0.27567407488822937, 0.055890120565891266],
      [0.5680539011955261, 0.055890120565891266],
      [0.5680539011955261, 0.0875505805015564],
      [0.27567407488822937, 0.0875505805015564],
    ],
    highlightAreas: {
      height: 120.49971051514149,
      left: 808.4142246097326,
      pageIndex: 0,
      top: 5.34892407804728,
      width: 859.8890691697598,
    },
  },
  {
    text: "Patient: Da",
    coordinates: [
      [0.10444720834493637, 0.22256237268447876],
      [0.20502308011054993, 0.22256237268447876],
      [0.20502308011054993, 0.2368307262659073],
      [0.10444720834493637, 0.2368307262659073],
    ],
    highlightAreas: {
      height: 54.305357275530696,
      left: 306.2914384715259,
      pageIndex: 0,
      top: 18.6034787774087,
      width: 295.793660774827,
    },
  },
  {
    text: "n",
    coordinates: [
      [0.30839717388153076, 0.22726765275001526],
      [0.31578096747398376, 0.22726765275001526],
      [0.31578096747398376, 0.2371787279844284],
      [0.30839717388153076, 0.2371787279844284],
    ],
    highlightAreas: {
      height: 54.305357275530696,
      left: 306.2914384715259,
      pageIndex: 0,
      top: 18.6034787774087,
      width: 295.793660774827,
    },
  },
  {
    text: "DOB:",
    coordinates: [
      [0.5146436095237732, 0.22474460303783417],
      [0.5674236416816711, 0.22474460303783417],
      [0.5674236416816711, 0.23836569488048553],
      [0.5146436095237732, 0.23836569488048553],
    ],
    highlightAreas: {
      height: 54.305357275530696,
      left: 306.2914384715259,
      pageIndex: 0,
      top: 18.6034787774087,
      width: 295.793660774827,
    },
  },
  {
    text: "Exam Date: 03/18/2005",
    coordinates: [
      [0.515426516532898, 0.24429188668727875],
      [0.7254267930984497, 0.24429188668727875],
      [0.7254267930984497, 0.25824347138404846],
      [0.515426516532898, 0.25824347138404846],
    ],
    highlightAreas: {
      width: 357.000495493412,
      height: 30.693482235074043,
      left: 876.2250781059265,
      pageIndex: 0,
      top: 20.6034787774087,
    },
  },
  {
    text: "Physician: Dr. Ye",
    coordinates: [
      [0.10295727849006653, 0.28108301758766174],
      [0.2559192180633545, 0.28108301758766174],
      [0.2559192180633545, 0.2968254089355469],
      [0.10295727849006653, 0.2968254089355469],
    ],
    highlightAreas: {
      width: 260.03529727458954,
      height: 34.63324047625065,
      left: 175.0273734331131,
      top: 24.3826386928558,
      pageIndex: 0,
    },
  },
  {
    text: "MRI OF THE BRAIN",
    coordinates: [
      [0.10291324555873871, 0.3389447033405304],
      [0.30435165762901306, 0.3389447033405304],
      [0.30435165762901306, 0.3527279496192932],
      [0.10291324555873871, 0.3527279496192932],
    ],
    highlightAreas: {
      width: 342.4453005194664,
      height: 30.323119275271893,
      left: 174.9525174498558,
      pageIndex: 0,
      top: 28.6034787774087,
    },
  },
  {
    text: "The study was performed on an AIRIS II magnetic resonance imaging system.",
    coordinates: [
      [0.10267813503742218, 0.3769610524177551],
      [0.7656673789024353, 0.3769610524177551],
      [0.7656673789024353, 0.3957929015159607],
      [0.10267813503742218, 0.3957929015159607],
    ],
    highlightAreas: {
      width: 1127.0817399024963,
      height: 41.43005572259426,
      left: 174.5528295636177,
      pageIndex: 0,
      top: 33.6034787774087,
    },
  },
  {
    text: "Technique: 1. Axial T2-weighted sequence",
    coordinates: [
      [0.10229228436946869, 0.41556012630462646],
      [0.48544061183929443, 0.41556012630462646],
      [0.48544061183929443, 0.43335312604904175],
      [0.10229228436946869, 0.43335312604904175],
    ],
    highlightAreas: {
      width: 651.3521313667297,
      height: 39.144591242074966,
      left: 173.89688342809677,
      pageIndex: 0,
      top: 38.6034787774087,
    },
  },
  {
    text: "2. Axial FLAIR sequence",
    coordinates: [
      [0.21900016069412231, 0.43553727865219116],
      [0.4391116797924042, 0.43553727865219116],
      [0.4391116797924042, 0.4524780213832855],
      [0.21900016069412231, 0.4524780213832855],
    ],
    highlightAreas: {
      width: 374.1896077990532,
      height: 37.269654497504234,
      left: 372.30027318000793,
      pageIndex: 0,
      top: 40.6034787774087,
    },
  },
  {
    text: "3. Sagittal T1-weighted sequence",
    coordinates: [
      [0.21903729438781738, 0.4548301696777344],
      [0.5025596618652344, 0.4548301696777344],
      [0.5025596618652344, 0.4719080626964569],
      [0.21903729438781738, 0.4719080626964569],
    ],
    highlightAreas: {
      width: 481.9880247116089,
      height: 37.57140971720219,
      left: 372.36340045928955,
      pageIndex: 0,
      top: 41.6034787774087,
    },
  },
  {
    text: "4. Coronal T1-weighted sequence",
    coordinates: [
      [0.2180936634540558, 0.47398537397384644],
      [0.5059815049171448, 0.47398537397384644],
      [0.5059815049171448, 0.4915492534637451],
      [0.2180936634540558, 0.4915492534637451],
    ],
    highlightAreas: {
      width: 489.40938115119934,
      height: 38.64050209522247,
      left: 370.75922787189484,
      pageIndex: 0,
      top: 42.6034787774087,
    },
  },
  {
    text: "Findings:",
    coordinates: [
      [0.1021210253238678, 0.512650728225708],
      [0.1864248514175415, 0.512650728225708],
      [0.1864248514175415, 0.5291279554367065],
      [0.1021210253238678, 0.5291279554367065],
    ],
    highlightAreas: {
      width: 143.31649169325829,
      height: 36.24999821186066,
      left: 173.60574305057526,
      pageIndex: 0,
      top: 45.6034787774087,
    },
  },
  {
    text: "Ventricular system appears to be normal. There is no mass effect or shift of midline",
    coordinates: [
      [0.10085678845643997, 0.5512000322341919],
      [0.8107959032058716, 0.5512000322341919],
      [0.8107959032058716, 0.5681471824645996],
      [0.10085678845643997, 0.5681471824645996],
    ],
    highlightAreas: {
      width: 1206.8965077400208,
      height: 37.28381246328354,
      left: 171.45654037594795,
      pageIndex: 0,
      top: 47.6034787774087,
    },
  },
  {
    text: "structures. I do not see any subdural collections. Cortical sulci and gyri appear to be",
    coordinates: [
      [0.1009153202176094, 0.5711713433265686],
      [0.8115425109863281, 0.5711713433265686],
      [0.8115425109863281, 0.5895413756370544],
      [0.1009153202176094, 0.5895413756370544],
    ],
    highlightAreas: {
      width: 1208.0662369728088,
      height: 40.41406288743019,
      left: 171.556044369936,
      pageIndex: 0,
      top: 48.6034787774087,
    },
  },
  {
    text: "normal.",
    coordinates: [
      [0.10088463127613068, 0.5897959470748901],
      [0.16543810069561005, 0.5897959470748901],
      [0.16543810069561005, 0.6033982038497925],
      [0.10088463127613068, 0.6033982038497925],
    ],
    highlightAreas: {
      width: 109.74091067910194,
      height: 29.925048910081387,
      left: 171.50387316942215,
      pageIndex: 0,
      top: 49.6034787774087,
    },
  },
  {
    text: "There are no areas of abnormal signal intensity within the white or gray matter of cerebral",
    coordinates: [
      [0.1010558009147644, 0.6290154457092285],
      [0.86284339427948, 0.6290154457092285],
      [0.86284339427948, 0.6474139094352722],
      [0.1010558009147644, 0.6474139094352722],
    ],
    highlightAreas: {
      width: 1295.0389087200165,
      height: 40.476562827825546,
      left: 171.7948615550995,
      pageIndex: 0,
      top: 55.6034787774087,
    },
  },
  {
    text: "or cerebellar hemispheres. There are no abnormalities at cervicocranial junction,",
    coordinates: [
      [0.10087316483259201, 0.648398220539093],
      [0.7823678255081177, 0.648398220539093],
      [0.7823678255081177, 0.6667455434799194],
      [0.10087316483259201, 0.6667455434799194],
    ],
    highlightAreas: {
      width: 1158.5409104824066,
      height: 40.364135056734085,
      left: 171.48438021540642,
      pageIndex: 0,
      top: 56.6034787774087,
    },
  },
  {
    text: "suprasellar cistern or quadrigeminal plate cistern.",
    coordinates: [
      [0.10110621154308319, 0.6677433848381042],
      [0.5171228647232056, 0.6677433848381042],
      [0.5171228647232056, 0.684894859790802],
      [0.10110621154308319, 0.684894859790802],
    ],
    highlightAreas: {
      width: 707.2283357381821,
      height: 37.733277678489685,
      left: 171.88055962324142,
      top: 57.0354466438293,
      pageIndex: 0,
    },
  },
  {
    text: "Incidentally, a large polyp or retention cyst is noted within left maxillary antrum with",
    coordinates: [
      [0.10043508559465408, 0.7060936093330383],
      [0.8227980136871338, 0.7060936093330383],
      [0.8227980136871338, 0.724190890789032],
      [0.10043508559465408, 0.724190890789032],
    ],
    highlightAreas: {
      width: 1228.0169904232025,
      height: 39.814088866114616,
      left: 170.73964551091194,
      pageIndex: 0,
      top: 63.6034787774087,
    },
  },
  {
    text: "evidence of mucous membrane reaction within the frontal sinuses and right maxillary",
    coordinates: [
      [0.10057076811790466, 0.725426435470581],
      [0.8245977163314819, 0.725426435470581],
      [0.8245977163314819, 0.7440416812896729],
      [0.10057076811790466, 0.7440416812896729],
    ],
    highlightAreas: {
      width: 1230.8458626270294,
      height: 40.95361456274986,
      left: 170.97030580043793,
      pageIndex: 0,
      top: 64.6034787774087,
    },
  },
  {
    text: "antrum. There is also evidence of inflammatory debris within a number of ethmoid air",
    coordinates: [
      [0.10089061409235, 0.7450590133666992],
      [0.8294965028762817, 0.7450590133666992],
      [0.8294965028762817, 0.7622357606887817],
      [0.10089061409235, 0.7622357606887817],
    ],
    highlightAreas: {
      width: 1238.629972934723,
      height: 37.78881952166557,
      left: 171.514043956995,
      pageIndex: 0,
      top: 65.6034787774087,
    },
  },
  {
    text: "cells on both sides.",
    coordinates: [
      [0.10012423992156982, 0.7644164562225342],
      [0.260063499212265, 0.7644164562225342],
      [0.260063499212265, 0.7777320742607117],
      [0.10012423992156982, 0.7777320742607117],
    ],
    highlightAreas: {
      width: 271.8967407941818,
      height: 29.294433444738388,
      left: 170.2112078666687,
      pageIndex: 0,
      top: 66.6034787774087,
    },
  },
  {
    text: "Conclusion:",
    coordinates: [
      [0.09976579248905182, 0.8033233284950256],
      [0.20662376284599304, 0.8033233284950256],
      [0.20662376284599304, 0.8169169425964355],
      [0.09976579248905182, 0.8169169425964355],
    ],
    highlightAreas: {
      width: 181.65854960680008,
      height: 29.90600634366274,
      left: 169.6018472313881,
      pageIndex: 0,
      top: 70.6034787774087,
    },
  },
  {
    text: "1.",
    coordinates: [
      [0.10011826455593109, 0.842208206653595],
      [0.1158803328871727, 0.842208206653595],
      [0.1158803328871727, 0.8551253080368042],
      [0.10011826455593109, 0.8551253080368042],
    ],
    highlightAreas: {
      width: 26.795516163110733,
      height: 28.41760255396366,
      left: 170.20104974508286,
      pageIndex: 0,
      top: 72.6034787774087,
    },
  },
  {
    text: "Unremarkable Magnetic Resonance Imaging of brain.",
    coordinates: [
      [0.15728823840618134, 0.8422452211380005],
      [0.6460428237915039, 0.8422452211380005],
      [0.6460428237915039, 0.8601837754249573],
      [0.15728823840618134, 0.8601837754249573],
    ],
    highlightAreas: {
      width: 830.8828204870224,
      height: 39.4648440182209,
      left: 267.39000529050827,
      pageIndex: 0,
      top: 73.6034787774087,
    },
  },
  {
    text: "2.",
    coordinates: [
      [0.0990670770406723, 0.8612188696861267],
      [0.11552542448043823, 0.8612188696861267],
      [0.11552542448043823, 0.874342143535614],
      [0.0990670770406723, 0.874342143535614],
    ],
    highlightAreas: {
      width: 27.979187481105328,
      height: 28.87121681123972,
      left: 168.4140309691429,
      pageIndex: 0,
      top: 74.6034787774087,
    },
  },
  {
    text: "Sinus disease as described above.",
    coordinates: [
      [0.15750785171985626, 0.861405611038208],
      [0.45595553517341614, 0.861405611038208],
      [0.45595553517341614, 0.8751376867294312],
      [0.15750785171985626, 0.8751376867294312],
    ],
    highlightAreas: {
      width: 507.36103653907776,
      height: 30.21044973284006,
      left: 267.76334792375565,
      pageIndex: 0,
      top: 75.6034787774087,
    },
  },
  {
    text: "000001",
    coordinates: [
      [0.8743640780448914, 0.9384810924530029],
      [0.9402234554290771, 0.9384810924530029],
      [0.9402234554290771, 0.9518270492553711],
      [0.8743640780448914, 0.9518270492553711],
    ],
    highlightAreas: {
      width: 111.96094155311584,
      height: 29.36108447611332,
      left: 1486.4189326763153,
      pageIndex: 0,
      top: 80.6034787774087,
    },
  },
  {
    text: "2",
    coordinates: [
      [0.8932558298110962, 0.9610438346862793],
      [0.9083643555641174, 0.9610438346862793],
      [0.9083643555641174, 0.9779413342475891],
      [0.8932558298110962, 0.9779413342475891],
    ],
    highlightAreas: {
      height: 1.7361139653599231,
      left: 88.66708376328415,
      pageIndex: 0,
      top: 93.70576485191243,
      width: 4.3614299543631585,
    },
  }, 
];
