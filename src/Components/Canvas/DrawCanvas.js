import React from "react";
import { LayerRenderStatus } from "@react-pdf-viewer/core";
import { sampleData } from "../../utils/MockData";
const DrawCanvas = () => {
  const onCanvasLayerRender = (e) => {
    // Return if the canvas isn't rendered completely
    if (e.status !== LayerRenderStatus.DidRender) {
      return;
    }
    // `e.ele` is the canvas element
    const canvas = e.ele;

    const ctx = canvas.getContext("2d");

    const style = { borderColor: "red", borderWidth: 2 };

    const { borderColor = "black", borderWidth = 1 } = style;

    ctx.beginPath();
    ctx.strokeStyle = borderColor;
    ctx.lineWidth = borderWidth;

    const height = JSON.parse(JSON.stringify(e.ele.height));
    const width = JSON.parse(JSON.stringify(e.ele.width));
    const scale = JSON.parse(JSON.stringify(e.scale));

    // we can multiply with e.scale to get coordinates with respect to each page
    sampleData.map((item) => {
      const x = ((item.left * width) / scale) * scale;
      const y = ((item.top * height) / scale) * scale;
      const w = ((item.width * width) / scale) * scale;
      const h = ((item.height * height) / scale) * scale;
      if (e.pageIndex == item.pageIndex) ctx.rect(x, y, w, h);
    });
    ctx.stroke();
  };

  return {
    onCanvasLayerRender,
  };
};

export default DrawCanvas;
