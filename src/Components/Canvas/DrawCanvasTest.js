import React,{useEffect, useState} from "react";
import { LayerRenderStatus } from "@react-pdf-viewer/core";
import { sampleData } from "../../utils/MockData";

const DrawCanvasTest = (data) => {

  const [final,setFinal]=useState([]);
  useEffect(()=>{
    setFinal(sampleData);
  },[final])
    
  const onCanvasLayerRender = (e) => {

    let result=data()?.map((value)=>{
      let remain=final?.filter((dat)=>
        JSON.stringify(dat) !== JSON.stringify(value));
      return remain;
    })
    setFinal(result);
    // Return if the canvas isn't rendered completely
    if (e.status !== LayerRenderStatus.DidRender) {
      return;
    }

    // `e.ele` is the canvas element
    const canvas = e.ele;

    const ctx = canvas.getContext("2d");

    const style = { borderColor: "gray", borderWidth: 2 };

    const { borderColor , borderWidth  } = style;

    ctx.beginPath();
    ctx.strokeStyle = borderColor;
    ctx.lineWidth = borderWidth;

    const height = JSON.parse(JSON.stringify(e.ele.height));
    const width = JSON.parse(JSON.stringify(e.ele.width));
    const scale = JSON.parse(JSON.stringify(e.scale));

    // we can multiply with e.scale to get coordinates with respect to each page
    final?.map((item) => {
      const x = ((item.left * width) / scale) * scale;
      const y = ((item.top * height) / scale) * scale;
      const w = ((item.width * width) / scale) * scale;
      const h = ((item.height * height) / scale) * scale;
      if (e.pageIndex == item.pageIndex)
       ctx.rect(x, y, w, h);
    });
    ctx.stroke();


    const style1 = data()?{ borderColor1: "green", borderWidth1: 2 }:{ borderColor1: "blue", borderWidth1: 2 }

    const { borderColor1, borderWidth1  } = style1;

    ctx.beginPath();
    ctx.strokeStyle = borderColor1;
    ctx.lineWidth = borderWidth1;

    data().map((item) => {
      const x = ((item.left * width) / scale) * scale;
      const y = ((item.top * height) / scale) * scale;
      const w = ((item.width * width) / scale) * scale;
      const h = ((item.height * height) / scale) * scale;
      if (e.pageIndex == item.pageIndex)
       ctx.rect(x, y, w, h);
    });
    ctx.stroke();
  };

  return {
    onCanvasLayerRender,
  };
};

export default DrawCanvasTest;
