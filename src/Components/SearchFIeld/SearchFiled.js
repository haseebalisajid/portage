import React from 'react'
import './SearchField.css';

const SearchFiled = ({handleChange,handleKeyDown}) => {
  return (
    <div className="searchField">
        <input
        type="text"
        className="input"
        placeholder="Enter the search Term(s)..."
        onChange={handleChange}
        onKeyDown={handleKeyDown}
        />
    </div>
  )
}

export default SearchFiled