import * as React from "react";
import { createStore } from "@react-pdf-viewer/core";

const CustomZoomPlugin = () => {
  const store = React.useMemo(() => createStore(), []);

  return {
    install: (pluginFunctions) => {
      store.update("zoom", pluginFunctions.zoom);
    },
    zoomTo: (scale) => {
      const fn = store.get("zoom");
      if (fn) {
        fn(scale);
      }
    },
  };
};

export default CustomZoomPlugin;
